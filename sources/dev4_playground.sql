-- phpMyAdmin SQL Dump
-- version 4.0.10.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 28, 2015 at 06:26 PM
-- Server version: 5.5.40-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dev4_playground`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'blank.png',
  `position_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `party` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term_a` int(10) unsigned NOT NULL,
  `term_b` int(10) unsigned NOT NULL,
  `month` int(10) unsigned NOT NULL,
  `office_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elect` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=475 ;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `slug`, `avatar`, `position_id`, `country_id`, `state`, `capital`, `region`, `full_name`, `party`, `term_a`, `term_b`, `month`, `office_time`, `term`, `elect`, `created_at`, `updated_at`) VALUES
(1, 'chinwoke-mbadinuju', 'blank.png', '1', '1', '1', 'Akwa', 'South-East', 'Chinwoke Mbadinuju', '', 1999, 1990, 5, '1', '1', 'yes', '2015-06-13 13:43:18', '2015-06-13 18:17:04'),
(2, 'ike-nwachukwu-', 'blank.png', '2', '1', '5', '', 'N', 'Ike Nwachukwu ', '', 1999, 2003, 1, '1', '1', NULL, '2015-06-13 14:17:33', '2015-06-13 23:56:56'),
(3, 'bob-nwannunu', 'blank.png', '2', '1', '5', '', 'C', 'Bob Nwannunu', '', 1999, 2003, 1, '1', '1', NULL, '2015-06-13 14:18:46', '2015-06-13 23:57:16'),
(4, 'adolphus-wabara', 'blank.png', '2', '1', '5', '', 'S', 'Adolphus Wabara', '', 1999, 2003, 1, '1', '1', NULL, '2015-06-13 14:22:09', '2015-06-13 23:57:37'),
(5, 'iya-abubakar', 'blank.png', '3', '1', '4', '', 'N', 'Iya Abubakar', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:22:59', '2015-06-13 14:22:59'),
(6, 'abubakar-girei', 'blank.png', '3', '1', '4', '', 'C', 'Abubakar Girei', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:24:15', '2015-06-13 14:24:15'),
(7, 'jonathan', 'blank.png', '3', '1', '4', '', 'S', 'Jonathan', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:30:21', '2015-06-13 14:30:21'),
(8, 'emmanuel', 'blank.png', '3', '1', '3', '', 'NE', 'Emmanuel', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:30:56', '2015-06-13 14:30:56'),
(9, 'udoma', 'blank.png', '3', '1', '3', '', 'NW', 'Udoma', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:31:34', '2015-06-13 14:31:34'),
(10, 'john-james-akpan', 'blank.png', '3', '0', '3', '', 'Uyo', 'John James Akpan', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:32:15', '2015-06-13 14:32:15'),
(11, 'chuba-okadigbo', 'blank.png', '3', '0', '1', '', 'N', 'Chuba Okadigbo', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:33:30', '2015-06-13 14:33:30'),
(12, 'mike-ajegbo', 'blank.png', '3', '0', '1', '', 'C', 'Mike Ajegbo', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:34:08', '2015-06-13 14:34:08'),
(13, 'nnamdi-eriobuna', 'blank.png', '3', '0', '1', '', 'S', 'Nnamdi Eriobuna', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:34:39', '2015-06-13 14:34:39'),
(14, 'bashir-mustapha', 'blank.png', '3', '0', '6', '', 'N', 'Bashir Mustapha', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:35:12', '2015-06-13 14:35:12'),
(15, 'idi-othman', 'blank.png', '3', '0', '6', '', 'C', 'Idi Othman', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:35:49', '2015-06-13 14:35:49'),
(16, 'salisu-matori', 'blank.png', '3', '0', '6', '', 'S', 'Salisu Matori', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:36:27', '2015-06-13 14:36:27'),
(17, 'tupele-ebi-diffa', 'blank.png', '3', '1', '7', '', 'W', 'Tupele-Ebi Diffa', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:37:12', '2015-06-13 14:37:12'),
(18, 'david-brigidi', 'blank.png', '3', '1', '7', '', 'C', 'David Brigidi', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:37:47', '2015-06-13 14:37:47'),
(19, 'melford-okilo', 'blank.png', '3', '0', '7', '', 'E', 'Melford Okilo', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:38:20', '2015-06-13 14:38:20'),
(20, 'david-mark', 'blank.png', '3', '1', '8', '', 'S', 'David Mark', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:39:54', '2015-06-13 14:39:54'),
(21, 'daniel-saror', 'blank.png', '3', '0', '8', '', 'N', 'Daniel Saror', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:41:36', '2015-06-13 14:41:36'),
(22, 'joseph-waku', 'blank.png', '3', '1', '8', '', 'C', 'Joseph Waku', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:42:19', '2015-06-13 14:42:19'),
(23, 'maina-maaji', 'blank.png', '3', '1', '10', '', 'N', 'Maina Maaji', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:43:22', '2015-06-13 14:43:22'),
(24, 'ali-modu', 'blank.png', '3', '1', '10', '', 'C', 'Ali Modu', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:43:56', '2015-06-13 14:43:56'),
(25, 'abubakar-mahdi', 'blank.png', '3', '1', '10', '', 'S', 'Abubakar Mahdi', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:44:33', '2015-06-13 14:44:33'),
(26, 'john-james', 'blank.png', '3', '1', '11', '', 'N', 'John James', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:45:27', '2015-06-13 14:45:27'),
(27, 'matthew-mbu', 'blank.png', '3', '1', '11', '', 'C', 'Matthew Mbu', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:47:34', '2015-06-13 14:47:34'),
(28, 'florence-ita', 'blank.png', '3', '1', '11', '', 'S', 'Florence Ita', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:48:09', '2015-06-13 14:48:09'),
(29, 'patrick-osakwe', 'blank.png', '3', '1', '12', '', 'N', 'Patrick Osakwe', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:49:13', '2015-06-13 14:49:13'),
(30, 'fred-aghogho', 'blank.png', '3', '1', '12', '', 'C', 'Fred Aghogho', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:49:45', '2015-06-13 14:49:45'),
(31, 'stella-omu', 'blank.png', '3', '1', '12', '', 'S', 'Stella Omu', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:50:23', '2015-06-13 14:50:23'),
(32, 'sylvanus-ngele', 'blank.png', '3', '1', '13', '', 'N', 'Sylvanus Ngele', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:51:13', '2015-06-13 14:51:13'),
(33, 'vincent-obasi', 'blank.png', '3', '1', '13', '', 'C', 'Vincent Obasi', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:51:52', '2015-06-13 14:51:52'),
(34, 'anyim-pius', 'blank.png', '3', '1', '13', '', 'S', 'Anyim Pius', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:52:45', '2015-06-13 14:52:45'),
(35, 'victor-oyofo', 'blank.png', '3', '1', '14', '', 'N', 'Victor Oyofo', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:53:23', '2015-06-13 14:53:23'),
(36, 'oserheimen-osunbor', 'blank.png', '3', '1', '14', '', 'C', 'Oserheimen Osunbor', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:53:58', '2015-06-13 14:53:58'),
(37, 'roland-owie', 'blank.png', '3', '1', '14', '', 'S', 'Roland Owie', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:54:43', '2015-06-13 14:54:43'),
(38, 'joseph-olatunji', 'blank.png', '3', '1', '15', '', 'N', 'Joseph Olatunji', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:55:29', '2015-06-13 14:55:29'),
(39, 'ayo-oni', 'blank.png', '3', '1', '15', '', 'C', 'Ayo Oni', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:56:25', '2015-06-13 14:56:25'),
(40, 'gbenga-aluko', 'blank.png', '3', '1', '15', '', 'S', 'Gbenga Aluko', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:57:08', '2015-06-13 14:57:08'),
(41, 'jim-nwobodo', 'blank.png', '3', '1', '2', '', 'E', 'Jim Nwobodo', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 14:57:50', '2015-06-13 14:57:50'),
(42, 'hyde-onuaguluchi', 'blank.png', '3', '1', '2', '', 'W', 'Hyde Onuaguluchi', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:00:16', '2015-06-13 15:00:16'),
(43, 'ben-collins-ndu', 'blank.png', '3', '1', '2', '', '', 'Ben-Collins Ndu', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:00:58', '2015-06-13 15:00:58'),
(44, 'fidelis', 'blank.png', '3', '1', '2', '', 'N', 'Fidelis', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:01:28', '2015-06-13 15:01:28'),
(45, 'umar-usman', 'blank.png', '3', '1', '17', '', 'N', 'Umar Usman', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:02:09', '2015-06-13 15:02:09'),
(46, 'saidu-kumo', 'blank.png', '3', '1', '17', '', 'C', 'Saidu Kumo', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:02:46', '2015-06-13 15:02:46'),
(47, 'idris-abubakar', 'blank.png', '3', '1', '17', '', 'S', 'Idris Abubakar', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:03:21', '2015-06-13 15:03:21'),
(48, 'ifeanyi-ararume', 'blank.png', '3', '1', '18', '', 'Owerri', 'Ifeanyi Ararume', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:03:53', '2015-06-13 15:03:53'),
(49, 'evan-enwerem', 'blank.png', '3', '1', '18', '', 'Okigwe', 'Evan Enwerem', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:04:35', '2015-06-13 15:04:35'),
(50, 'arthur-nzeribe', 'blank.png', '3', '1', '18', '', 'Orlu', 'Arthur Nzeribe', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:05:10', '2015-06-13 15:05:10'),
(51, 'maitama-bello', 'blank.png', '3', '1', '19', '', 'C', 'Maitama Bello', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:05:40', '2015-06-13 15:05:40'),
(52, 'ibrahim-muhammed', 'blank.png', '3', '1', '19', '', 'NE', 'Ibrahim Muhammed', 'Jigawa', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:06:13', '2015-06-13 15:06:13'),
(53, 'mohammed-alkali', 'blank.png', '3', '1', '19', '', 'NW', 'Mohammed Alkali', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:11:22', '2015-06-13 15:11:22'),
(54, 'dalhatu-tafida', 'blank.png', '3', '1', '20', '', 'N', 'Dalhatu Tafida', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:28:10', '2015-06-13 15:28:10'),
(55, 'dalhatu-tafida', 'blank.png', '3', '1', '20', '', 'N', 'Dalhatu Tafida', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:28:11', '2015-06-13 15:28:11'),
(56, 'mohammed-aruwa', 'blank.png', '3', '1', '20', '', 'C', 'Mohammed Aruwa', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:38:14', '2015-06-13 15:38:14'),
(57, 'haruna-aziz', 'blank.png', '3', '1', '21', '', 'N', 'Haruna Aziz', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:40:08', '2015-06-13 15:40:08'),
(58, 'ibrahim-kura', 'blank.png', '3', '1', '21', '', 'C', 'Ibrahim Kura', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:40:46', '2015-06-13 15:40:46'),
(59, 'mas’ud-el-jibril', 'blank.png', '3', '1', '21', '', 'S', 'Mas’ud El-Jibril', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:42:35', '2015-06-13 15:42:35'),
(60, 'abdul-yandoma', 'blank.png', '3', '1', '22', '', 'N', 'Abdul Yandoma', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:50:29', '2015-06-13 15:50:29'),
(61, 'sama’ila-mamman', 'blank.png', '3', '1', '22', '', 'C', 'Sama’ila Mamman', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 15:59:48', '2015-06-13 15:59:48'),
(62, 'mohammed-tukur', 'blank.png', '3', '1', '22', '', 'S', 'Mohammed Tukur', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:01:26', '2015-06-13 16:01:26'),
(63, 'adamu-augie', 'blank.png', '3', '1', '23', '', 'N', 'Adamu Augie', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:02:48', '2015-06-13 16:02:48'),
(64, 'abubakar--abdullahi', 'blank.png', '3', '1', '23', '', 'C', 'Abubakar  Abdullahi', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:04:33', '2015-06-13 16:04:33'),
(65, 'danladi-bamaiyi', 'blank.png', '3', '1', '23', '', 'S', 'Danladi Bamaiyi', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:14:15', '2015-06-13 16:14:15'),
(66, 'ahmed-tijani', 'blank.png', '3', '1', '24', '', 'C', 'Ahmed Tijani', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:18:52', '2015-06-13 16:18:52'),
(67, 'tunde-ogbeha', 'blank.png', '3', '1', '24', '', 'W', 'Tunde Ogbeha', 'Ogbeha', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:19:39', '2015-06-13 16:19:39'),
(68, 'alex-kadiri', 'blank.png', '3', '1', '24', '', 'E', 'Alex Kadiri', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:27:25', '2015-06-13 16:27:25'),
(69, 'ahmed-zuruq', 'blank.png', '3', '1', '25', '', 'N', 'Ahmed Zuruq', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:29:29', '2015-06-13 16:29:29'),
(70, 'salman-is''haq', 'blank.png', '3', '1', '25', '', 'C', 'Salman Is''haq', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:30:02', '2015-06-13 16:30:02'),
(71, 'suleiman-ajadi', 'blank.png', '3', '1', '25', '', 'S', 'Suleiman Ajadi', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:30:39', '2015-06-13 16:30:39'),
(72, 'adeseye-ogunlewe', 'blank.png', '3', '1', '26', '', 'E', 'Adeseye Ogunlewe', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:33:47', '2015-06-13 16:33:47'),
(73, 'wahab-dosunmu', 'blank.png', '3', '1', '26', '', 'C', 'Wahab Dosunmu', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:34:18', '2015-06-13 16:34:18'),
(74, 'tokunbo-afikuyomi', 'blank.png', '3', '1', '26', '', 'W', 'Tokunbo Afikuyomi', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:34:49', '2015-06-13 16:34:49'),
(75, 'patrick-aga', 'blank.png', '3', '1', '27', '', 'N', 'Patrick Aga', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:35:20', '2015-06-13 16:35:20'),
(76, 'abubakar-sodangi', 'blank.png', '3', '1', '27', '', 'W', 'Abubakar Sodangi', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:35:54', '2015-06-13 16:35:54'),
(77, 'haruna-abubakar', 'blank.png', '3', '1', '27', '', 'S', 'Haruna Abubakar', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:36:29', '2015-06-13 16:36:29'),
(78, 'ibrahim-kuta', 'blank.png', '3', '1', '28', '', 'N', 'Ibrahim Kuta', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:37:04', '2015-06-13 16:37:04'),
(79, 'isa-mohammed', 'blank.png', '3', '1', '28', '', 'C', 'Isa Mohammed', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:37:38', '2015-06-13 16:37:38'),
(80, 'nuhu-aliyu-', 'blank.png', '3', '1', '28', '', 'S', 'Nuhu Aliyu ', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:39:13', '2015-06-13 16:39:13'),
(81, 'afolabi-olabimtan', 'blank.png', '3', '1', '30', '', 'C', 'Afolabi Olabimtan', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:43:34', '2015-06-13 16:43:34'),
(82, 'femi-okurounmu', 'blank.png', '3', '1', '30', '', 'W', 'Femi Okurounmu', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:44:10', '2015-06-13 16:44:10'),
(83, 'olabiyi-durojaiye', 'blank.png', '3', '1', '30', '', 'E', 'Olabiyi Durojaiye', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:44:47', '2015-06-13 16:44:47'),
(84, 'lawrence-ayo', 'blank.png', '3', '1', '', '', 'N', 'Lawrence Ayo', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:46:31', '2015-06-13 16:46:31'),
(85, 'gbenga-ogunniya', 'blank.png', '3', '1', '', '', 'C', 'Gbenga Ogunniya', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:47:06', '2015-06-13 16:47:06'),
(86, 'omololu-meroyi', 'blank.png', '3', '1', '', '', 'S', 'Omololu Meroyi', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:47:34', '2015-06-13 16:47:34'),
(87, 'moji-akinfenwa', 'blank.png', '3', '1', '32', '', 'N', 'Moji Akinfenwa', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:48:05', '2015-06-13 16:48:05'),
(88, 'sunday-fajinmi', 'blank.png', '3', '1', '32', '', 'C', 'Sunday Fajinmi', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:48:39', '2015-06-13 16:48:39'),
(89, 'adebayo-salami', 'blank.png', '3', '1', '32', '', 'S', 'Adebayo Salami', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:49:07', '2015-06-13 16:49:07'),
(90, 'brimmo-yusuf', 'blank.png', '3', '1', '33', '', 'N', 'Brimmo Yusuf', '(AD)', 1999, 2004, 1, '', '0', NULL, '2015-06-13 16:49:44', '2015-06-13 16:49:44'),
(91, 'lekan-balogun', 'blank.png', '3', '1', '33', '', 'C', 'Lekan Balogun', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:50:15', '2015-06-13 16:50:15'),
(92, 'peter-olawuyi', 'blank.png', '3', '1', '33', '', 'S', 'Peter Olawuyi', '(AD)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:50:41', '2015-06-13 16:50:41'),
(93, 'davou-zang', 'blank.png', '3', '1', '34', '', 'N', 'Davou Zang', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:51:20', '2015-06-13 16:51:20'),
(94, 'ibrahim-mantu', 'blank.png', '3', '1', '34', '', 'C', 'Ibrahim Mantu', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:51:50', '2015-06-13 16:51:50'),
(95, 'silas-janfa', 'blank.png', '3', '1', '34', '', 'S', 'Silas Janfa', '', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:52:28', '2015-06-13 16:52:28'),
(96, 'silas-janfa', 'blank.png', '3', '1', '34', '', 'S', 'Silas Janfa', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:52:36', '2015-06-13 16:52:36'),
(97, 'ibiapuye-martyns-yellowe', 'blank.png', '3', '1', '35', '', 'W', 'Ibiapuye Martyns-Yellowe', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:53:08', '2015-06-13 16:53:08'),
(98, 'john-azuta-mbata', 'blank.png', '3', '1', '35', '', 'E', 'John Azuta-Mbata', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:53:41', '2015-06-13 16:53:41'),
(99, 'adawari-pepple', 'blank.png', '3', '1', '35', '', 'SE', 'Adawari Pepple', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:54:19', '2015-06-13 16:54:19'),
(100, 'aliyu-abubakar', 'blank.png', '3', '1', '36', '', 'N', 'Aliyu Abubakar', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:54:55', '2015-06-13 16:54:55'),
(101, 'bello-jibrin', 'blank.png', '3', '1', '36', '', 'E', 'Bello Jibrin', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:55:29', '2015-06-13 16:55:29'),
(102, 'abdallah-wali', 'blank.png', '3', '1', '36', '', 'S', 'Abdallah Wali', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:57:26', '2015-06-13 16:57:26'),
(103, 'abdulahi-bala', 'blank.png', '3', '1', '36', '', 'N', 'Abdulahi Bala', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 16:58:47', '2015-06-13 16:58:47'),
(104, 'abdulazeez-ibrahim', 'blank.png', '3', '1', '36', '', 'C', 'Abdulazeez Ibrahim', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 17:00:54', '2015-06-13 17:00:54'),
(105, 'dalhatu-umaru', 'blank.png', '3', '1', '36', '', 'S', 'Dalhatu Umaru', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 17:01:27', '2015-06-13 17:01:27'),
(106, 'usman-albishir', 'blank.png', '3', '1', '38', '', 'N', 'Usman Albishir', '(APP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 17:02:04', '2015-06-13 17:02:04'),
(107, 'goni-modu-', 'blank.png', '0', '1', '38', '', 'C', 'Goni Modu ', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 17:02:37', '2015-06-13 17:02:37'),
(108, 'mamman-bello', 'blank.png', '3', '1', '38', '', 'S', 'Mamman Bello', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 17:06:02', '2015-06-13 17:06:02'),
(109, 'lawali-shuaibu', 'blank.png', '3', '1', '37', '', 'N', 'Lawali Shuaibu', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 17:06:44', '2015-06-13 17:06:44'),
(110, 'saidu-dansadau', 'blank.png', '3', '1', '37', '', 'C', 'Saidu Dansadau', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 17:08:08', '2015-06-13 17:08:08'),
(111, 'yushau-anka', 'blank.png', '3', '1', '37', '', 'W', 'Yushau Anka', '(ANPP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 17:08:39', '2015-06-13 17:08:39'),
(112, 'khairat-abdulrazaq-gwadabe', 'blank.png', '3', '1', '16', '', 'FCT', 'Khairat Abdulrazaq-Gwadabe', '(PDP)', 1999, 2003, 1, '', '0', NULL, '2015-06-13 17:09:05', '2015-06-13 17:09:05'),
(113, 'chinwoke-mbadinuju', 'blank.png', '1', '1', '1', 'Akwa', 'South-East', 'Chinwoke Mbadinuju', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:26:07', '2015-06-14 10:26:07'),
(114, 'chimaroke-nnamani', 'blank.png', '1', '1', '2', 'Enugu', 'South-East', 'Chimaroke Nnamani', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:27:14', '2015-06-14 10:27:14'),
(115, 'victor-attah', 'blank.png', '1', '1', '3', 'Uyo', 'South-South', 'Victor Attah', 'PDP', 1999, 0, 5, '4', '2', 'yes', '2015-06-14 10:28:04', '2015-06-14 10:28:04'),
(116, 'boni-haruna', 'blank.png', '1', '1', '4', 'Yola', 'North-East', 'Boni Haruna', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:31:55', '2015-06-14 10:31:55'),
(117, 'orji-uzor-kalu', 'blank.png', '1', '1', '5', 'Umuahia', 'South-East', 'Orji Uzor Kalu', 'PDP', 1999, 0, 3, '4', '1', 'yes', '2015-06-14 10:36:39', '2015-06-14 10:36:39'),
(118, 'adamu-mu''azu', 'blank.png', '1', '1', '6', 'Bauchi', 'North-East', 'Adamu Mu''azu', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:38:22', '2015-06-14 10:38:22'),
(119, 'diepreye-alamieyeseigha', 'blank.png', '1', '1', '7', 'Yenagoa', 'South-South', 'Diepreye Alamieyeseigha', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:39:22', '2015-06-14 10:39:22'),
(120, 'george-akume', 'blank.png', '1', '1', '8', 'Makurdi', 'Central', 'George Akume', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:40:40', '2015-06-14 10:40:40'),
(121, 'mala-kachalla', 'blank.png', '1', '1', '10', 'Maiduguri', 'North-East', 'Mala Kachalla', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:41:35', '2015-06-14 10:41:35'),
(122, 'donald-duke', 'blank.png', '1', '1', '11', 'Calabar', 'South-South', 'Donald Duke', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:42:41', '2015-06-14 10:42:41'),
(123, 'james-ibori', 'blank.png', '1', '1', '12', 'Asaba', 'South-South', 'James Ibori', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:44:23', '2015-06-14 10:44:23'),
(124, 'sam-egwu', 'blank.png', '1', '1', '13', 'Abakaliki', 'South-East', 'Sam Egwu', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:54:12', '2015-06-14 10:54:12'),
(125, 'chief-lucky-igbinedion', 'blank.png', '1', '1', '14', 'Benin-City', 'South-South', 'Chief Lucky Igbinedion', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:54:48', '2015-06-14 10:54:48'),
(126, 'niyi-adebayo', 'blank.png', '1', '1', '15', 'Ado Ekiti', 'South-West', 'Niyi Adebayo', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:55:52', '2015-06-14 10:55:52'),
(127, 'abubakar-habu-hashidu', 'blank.png', '1', '1', '17', 'Gombe', 'North-East', 'Abubakar Habu Hashidu', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:56:35', '2015-06-14 10:56:35'),
(128, 'achike-udenwa', 'blank.png', '1', '1', '18', 'Owerri', 'South-South', 'Achike Udenwa', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:57:20', '2015-06-14 10:57:20'),
(129, 'ibrahim-saminu-turaki', 'blank.png', '1', '1', '19', 'Dutse', 'North-Central', 'Ibrahim Saminu Turaki', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:57:52', '2015-06-14 10:57:52'),
(130, 'ahmed-makarfi', 'blank.png', '2', '1', '20', 'Kaduna', 'North-West', 'Ahmed Makarfi', 'PDP', 1999, 1990, 5, '4', '1', 'yes', '2015-06-14 10:58:37', '2015-06-17 09:58:51'),
(131, 'rabiu-musa-kwankwaso', 'blank.png', '1', '1', '21', 'Kano', 'North-West', 'Rabiu Musa Kwankwaso', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:59:11', '2015-06-14 10:59:11'),
(132, 'umaru-musa-yar''adua', 'blank.png', '1', '1', '22', 'Katsina', 'North', 'Umaru Musa Yar''Adua', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 10:59:50', '2015-06-14 10:59:50'),
(133, 'adamu-aliero', 'blank.png', '1', '1', '23', 'Birnin Kebbi', 'North-West', 'Adamu Aliero', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 11:00:32', '2015-06-14 11:00:32'),
(134, 'abubakar-audu', 'blank.png', '1', '1', '24', 'Lokoja', 'Central', 'Abubakar Audu', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 11:01:08', '2015-06-14 11:01:08'),
(135, 'mohammed-lawal', 'blank.png', '1', '1', '25', 'Illorin', 'West', 'Mohammed Lawal', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 11:53:28', '2015-06-14 11:53:28'),
(136, 'mr-bola-tinubu[', 'blank.png', '1', '1', '26', 'Ikeja', 'South-West', 'Mr Bola Tinubu[', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 11:55:59', '2015-06-14 11:55:59'),
(137, 'abdullahi-adamu', 'blank.png', '1', '1', '27', 'Lafia', 'Central', 'Abdullahi Adamu', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 11:56:48', '2015-06-14 11:56:48'),
(138, 'abdulkadir-kure', 'blank.png', '1', '1', '28', 'Minna', 'North-West', 'Abdulkadir Kure', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 11:57:17', '2015-06-14 11:57:17'),
(139, 'olusegun-osoba', 'blank.png', '1', '1', '30', 'Abeokuta', 'South-West', 'Olusegun Osoba', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 11:57:47', '2015-06-14 11:57:47'),
(140, 'adebayo-adefarati', 'blank.png', '1', '1', '31', 'Akure', 'South-West', 'Adebayo Adefarati', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 11:58:24', '2015-06-14 11:58:24'),
(141, 'adebisi-akande', 'blank.png', '1', '1', '32', 'Osogbo', 'South-West', 'Adebisi Akande', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 11:59:00', '2015-06-14 11:59:00'),
(142, 'dr.-lam-adesina', 'blank.png', '1', '1', '33', 'Ibadan', 'South-West', 'Dr. Lam Adesina', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 11:59:35', '2015-06-14 11:59:35'),
(143, 'peter-odili', 'blank.png', '1', '1', '35', 'Port Harcourt', 'South-South', 'Peter Odili', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 12:00:12', '2015-06-14 12:00:12'),
(144, 'attahiru-bafarawa', 'blank.png', '1', '1', '36', 'Sokoto', 'North-West', 'Attahiru Bafarawa', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 12:00:43', '2015-06-14 12:00:43'),
(145, 'jolly-nyame', 'blank.png', '1', '1', '36', 'Jalingo', 'East', 'Jolly Nyame', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 12:01:10', '2015-06-14 12:01:10'),
(146, 'bukar-abba-ibrahim', 'blank.png', '1', '1', '38', 'Damaturu', 'North-East', 'Bukar Abba Ibrahim', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 12:01:38', '2015-06-14 12:01:38'),
(147, 'ahmad-sani-yerima', 'blank.png', '1', '1', '37', 'Gusau', 'Noth-West', 'Ahmad Sani Yerima', 'PDP', 1999, 0, 5, '4', '1', 'yes', '2015-06-14 12:02:04', '2015-06-14 12:02:04'),
(148, 'iheanacho-obioma', 'blank.png', '2', '1', '', '', 'Ikwuano/Umuahia North/South', 'Iheanacho Obioma', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:04:18', '2015-06-14 12:04:18'),
(149, 'njoku-nnamdi', 'blank.png', '2', '1', '', '', 'Bende', 'Njoku Nnamdi', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:04:49', '2015-06-14 12:04:49'),
(150, 'uchechukwu-n.-maduako', 'blank.png', '2', '1', '', '', 'Isuikwato/Umunneochi', 'Uchechukwu N. Maduako', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:08:21', '2015-06-14 12:08:21'),
(151, 'mao-arukwe-ohuabunwa', 'blank.png', '2', '1', '5', '', 'Arochukwu/Ohafia', 'Mao Arukwe Ohuabunwa', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:09:01', '2015-06-14 12:09:01'),
(152, 'anthony-eze-enwereuzor', 'blank.png', '2', '1', '5', '', 'Aba North/South', 'Anthony Eze Enwereuzor', 'ANPP', 1999, 0, 1, '1', '0', NULL, '2015-06-14 12:10:44', '2015-06-14 12:10:44'),
(153, 'macebuh-chinonyerem', 'blank.png', '2', '1', '5', '', 'Ukwa East/Ukwa West', 'Macebuh Chinonyerem', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:15:25', '2015-06-14 12:15:25'),
(154, 'nwakanwa-chimaobi', 'blank.png', '2', '1', '5', '', 'Isiala Ngwa North/South', 'Nwakanwa Chimaobi', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:16:08', '2015-06-14 12:16:08'),
(155, 'clifford-ohiagu', 'blank.png', '2', '1', '5', '', 'Obingwa/Osisioma/Ugbunagbo', 'Clifford Ohiagu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:16:48', '2015-06-14 12:16:48'),
(156, 'akpan-eno-sampson', 'blank.png', '2', '1', '3', '', 'Ukanafun/Orukanam', 'Akpan Eno Sampson', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:17:43', '2015-06-14 12:17:43'),
(157, 'bassey-asuquoemah', 'blank.png', '2', '1', '3', '', 'Etinan', 'Bassey AsuquoEmah', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:18:32', '2015-06-14 12:18:32'),
(158, 'enang-ita-solomon', 'blank.png', '2', '1', '3', '', 'Itu/Ibiono Ibom', 'Enang Ita Solomon', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:19:20', '2015-06-14 12:19:20'),
(159, 'nduese-essien', 'blank.png', '2', '1', '3', '', 'Eket', 'Nduese Essien', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:19:49', '2015-06-14 12:19:49'),
(160, 'esu-tony-ibana', 'blank.png', '2', '1', '3', '', 'Ikot Ekpene/ Essien Udim/ Ubot Akara', 'Esu Tony Ibana', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:20:21', '2015-06-14 12:20:21'),
(161, 'eyo-akaninyene-eno', 'blank.png', '2', '1', '3', '', 'Abak', 'Eyo Akaninyene Eno', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:20:53', '2015-06-14 12:20:53'),
(162, 'minimah-iquo-inyang', 'blank.png', '2', '1', '3', '', 'Ikono/ Ini', 'Minimah Iquo-Inyang', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:21:17', '2015-06-14 12:21:17'),
(163, 'udoh-esio-oquong', 'blank.png', '2', '1', '3', '', 'Oron/Mbo/Okobo/UrueOffong/Oruko/Udung-Uko Const.', 'Udoh Esio Oquong', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:26:05', '2015-06-14 12:26:05'),
(164, 'udoh-bernard-ambrose', 'blank.png', '2', '1', '3', '', 'Ikot Abasi', 'Udoh Bernard Ambrose', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:26:38', '2015-06-14 12:26:38'),
(165, 'ukpong-emaeyak-nkana', 'blank.png', '2', '1', '3', '', 'Uyo/Uruan/Nsit Ata/Ibeskip Asutan', 'Ukpong Emaeyak Nkana', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:27:19', '2015-06-14 12:27:19'),
(166, 'anosike-emma-obiajulu', 'blank.png', '2', '1', '1', '', 'Anambra East/West', 'Anosike Emma Obiajulu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:27:49', '2015-06-14 12:27:49'),
(167, 'chukwuemeka-chikelu', 'blank.png', '2', '1', '1', '', 'Anaocha/Njikoka/Dunukofia', 'Chukwuemeka Chikelu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:28:19', '2015-06-14 12:28:19'),
(168, 'duru-chidi-okechukwu', 'blank.png', '2', '1', '1', '', 'Aguta', 'Duru Chidi Okechukwu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:28:50', '2015-06-14 12:28:50'),
(169, 'efobi-bertrand-maduka', 'blank.png', '2', '1', '1', '', 'Nnewi North/South/Ekwusigo', 'Efobi Bertrand Maduka', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:29:19', '2015-06-14 12:29:19'),
(170, 'ikpeazu-lynda-chuba', 'blank.png', '2', '1', '1', '', 'Onitsha North/South', 'Ikpeazu Lynda Chuba', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:29:53', '2015-06-14 12:29:53'),
(171, 'offodile-chudi', 'blank.png', '2', '1', '1', '', 'Awka North/South', 'Offodile Chudi', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:30:23', '2015-06-14 12:30:23'),
(172, 'okechukwu-udeh', 'blank.png', '2', '1', '1', '', 'Orumba North/South', 'Okechukwu Udeh', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:30:53', '2015-06-14 12:30:53'),
(173, 'okeke-frederick-a.u.', 'blank.png', '2', '1', '1', '', 'Ihiala', 'Okeke Frederick A.U.', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:31:19', '2015-06-14 12:31:19'),
(174, 'ughanze-celestine-nnaemeka', 'blank.png', '2', '1', '1', '', 'Oyi/Ayamelum', 'Ughanze Celestine Nnaemeka', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:31:47', '2015-06-14 12:31:47'),
(175, 'ugokwe-jerry-sonny', 'blank.png', '2', '1', '1', '', 'Idemili North/South', 'Ugokwe Jerry Sonny', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:32:15', '2015-06-14 12:32:15'),
(176, 'uzoka-okwudili', 'blank.png', '2', '1', '1', '', 'Ogbaru', 'Uzoka Okwudili', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:32:40', '2015-06-14 12:32:40'),
(177, 'andie-clement-t.', 'blank.png', '2', '1', '7', '', 'Sagbama/ekeremor', 'Andie Clement T.', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:33:07', '2015-06-14 12:33:07'),
(178, 'graham-ipigansi', 'blank.png', '2', '1', '7', '', 'Ogbia', 'Graham Ipigansi', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:33:41', '2015-06-14 12:33:41'),
(179, 'okoto-foster-bruce', 'blank.png', '2', '1', '7', '', 'Southern Ijaw', 'Okoto Foster Bruce', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:34:13', '2015-06-14 12:34:13'),
(180, 'torukurobo-epengule-mike', 'blank.png', '2', '1', '7', '', 'Bayelsa Central', 'Torukurobo Epengule Mike', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:34:38', '2015-06-14 12:34:38'),
(181, 'wuku-dieworio-wilson', 'blank.png', '2', '1', '7', '', 'Brass/Nembe', 'Wuku Dieworio Wilson', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:46:15', '2015-06-14 12:46:15'),
(182, 'agidani-solomon-umoru', 'blank.png', '2', '1', '8', '', 'Apa/Aguta', 'Agidani Solomon Umoru', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:50:20', '2015-06-14 12:50:20'),
(183, 'gbande-moses-akuha-tor', 'blank.png', '2', '1', '8', '', 'Kwande/Ushongo', 'Gbande Moses Akuha Tor', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:52:34', '2015-06-14 12:52:34'),
(184, 'idikwu-altine-oga', 'blank.png', '2', '1', '8', '', 'Igede', 'Idikwu Altine Oga', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 12:58:29', '2015-06-14 12:58:29'),
(185, 'jooji-tachia', 'blank.png', '2', '1', '8', '', 'Vandeikya/Konshisha', 'Jooji Tachia', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:02:59', '2015-06-14 13:02:59'),
(186, 'malherbe-zakarie-andiir', 'blank.png', '2', '1', '8', '', 'Buruku', 'Malherbe Zakarie Andiir', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:03:29', '2015-06-14 13:03:29'),
(187, 'obande-sameul-onazi', 'blank.png', '2', '1', '8', '', 'Ado/Ogbadigba/Opkokwu', 'Obande Sameul Onazi', 'PDP', 1999, 2003, 1, '1', '0', 'no', '2015-06-14 13:04:44', '2015-06-14 13:04:44'),
(188, 'shehu-shettima', 'blank.png', '2', '1', '8', '', 'Kaga/gubio/Magumeri', 'Shehu Shettima', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:05:24', '2015-06-14 13:05:24'),
(189, 'suswam-torwuagabriel', 'blank.png', '2', '1', '8', '', 'Katsina-Alu/Ukum/Logo', 'Suswam TorwuaGabriel', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:05:52', '2015-06-14 13:05:52'),
(190, 'terngu-tsegba', 'blank.png', '2', '1', '8', '', 'Gboko/Tarka', 'Terngu Tsegba', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:08:36', '2015-06-14 13:08:36'),
(191, 'upaa-cletus-tivnongu-kinga', 'blank.png', '2', '1', '8', '', 'Makurdi/Guma', 'Upaa Cletus Tivnongu Kinga', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:11:40', '2015-06-14 13:11:40'),
(192, 'asuquo-nya-eyoma', 'blank.png', '2', '1', '11', '', 'Calabar Munincipal/Odukpani', 'Asuquo Nya Eyoma', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:15:06', '2015-06-14 13:15:06'),
(193, 'igbodor-peter-leja', 'blank.png', '2', '1', '11', '', 'Ogoja/Yala', 'Igbodor Peter Leja', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:15:58', '2015-06-14 13:15:58'),
(194, 'nyambi-alobi-odey', 'blank.png', '2', '1', '11', '', 'Ikom/Boki', 'Nyambi Alobi Odey', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:16:23', '2015-06-14 13:16:23'),
(195, 'obeten-obeten-okon', 'blank.png', '2', '1', '11', '', 'Yakurr/Abi', 'Obeten Obeten Okon', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:16:51', '2015-06-14 13:16:51'),
(196, 'ogar-mike-o.', 'blank.png', '2', '1', '11', '', 'Bekwarra/Obudu/Obanliku', 'Ogar Mike O.', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:17:31', '2015-06-14 13:17:31'),
(197, 'okon-patrick-ene', 'blank.png', '2', '1', '11', '', 'Akpabuyo/Bakassi/Calabar South', 'Okon Patrick Ene', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:20:31', '2015-06-14 13:20:31'),
(198, 'orok-agbor-patrick', 'blank.png', '2', '1', '11', '', 'Akamkpa/Biase', 'Orok Agbor Patrick', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:21:04', '2015-06-14 13:21:04'),
(199, 'tangban-ebutaamba', 'blank.png', '2', '1', '11', '', 'Obubra/Etung', 'Tangban EbutaAmba', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:22:40', '2015-06-14 13:22:40'),
(200, 'adedayo-fajura-charles', 'blank.png', '2', '1', '15', '', ' IJERO/EFON/EKITI WEST', 'Adedayo Fajura Charles', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:23:05', '2015-06-14 13:23:05'),
(201, 'ajayi-joseph-aderemi', 'blank.png', '2', '1', '15', '', ' EMURE/GBONYIN/EKITI EAST', 'Ajayi Joseph Aderemi', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:23:32', '2015-06-14 13:23:32'),
(202, 'aladejebi-francis-oluyemi', 'blank.png', '2', '1', '15', '', ' EKITI NORTH', 'Aladejebi Francis Oluyemi', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:23:59', '2015-06-14 13:23:59'),
(203, 'olusegun-ojor.', 'blank.png', '2', '1', '15', '', ' ADO EKITI/IREPODUN-IFELODUN', 'Olusegun OjoR.', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:24:25', '2015-06-14 13:24:25'),
(204, 'owoola-ogunsanmi', 'blank.png', '2', '1', '15', '', ' EKITI II', 'Owoola Ogunsanmi', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:24:54', '2015-06-14 13:24:54'),
(205, 'ropo-ige', 'blank.png', '2', '1', '15', '', ' EKITI SOUTH WEST/IKERE/ISE -ORUN', 'Ropo Ige', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:25:23', '2015-06-14 13:25:23'),
(206, 'anyanwu-tony', 'blank.png', '2', '1', '18', '', 'Ahiazu Mbaise/Ezinihitte', 'Anyanwu Tony', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:25:54', '2015-06-14 13:25:54'),
(207, 'dike-cajethan-o.', 'blank.png', '2', '1', '18', '', 'Orlu/Oru East', 'Dike Cajethan O.', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:26:18', '2015-06-14 13:26:18'),
(208, 'egu-greg-c.', 'blank.png', '2', '1', '18', '', 'Aboh Mbaise/Ngor Okpala', 'Egu Greg C.', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:26:45', '2015-06-14 13:26:45'),
(209, 'eze-okere-tony', 'blank.png', '2', '1', '18', '', 'Ohaji/Egbema', 'Eze Okere Tony', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:27:08', '2015-06-14 13:27:08'),
(210, 'ezeani-nnamdi', 'blank.png', '2', '1', '18', '', 'Ideato North /South', 'Ezeani Nnamdi', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:27:32', '2015-06-14 13:27:32'),
(211, 'ibekwe-mauriceobasi', 'blank.png', '2', '1', '18', '', 'Okigwe North', 'Ibekwe MauriceObasi', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:27:55', '2015-06-14 13:27:55'),
(212, 'nwajiuba-chukwuemeka-u.', 'blank.png', '2', '1', '18', '', 'Ehimembano/ihitte Uboma/Obowo', 'Nwajiuba Chukwuemeka U.', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:28:16', '2015-06-14 13:28:16'),
(213, 'nwole-uchenna', 'blank.png', '2', '1', '18', '', 'Mbaitolu/ikeduru', 'Nwole Uchenna', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:28:42', '2015-06-14 13:28:42'),
(214, 'oguike-levi', 'blank.png', '2', '1', '18', '', 'Owerri Municipal/Owerri North/West', 'Oguike Levi', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:33:46', '2015-06-14 13:33:46'),
(215, 'osuala-christopher-chukwuemeka', 'blank.png', '2', '1', '18', '', 'Nwangele/Isu/Njaba', 'Osuala Christopher Chukwuemeka', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:34:56', '2015-06-14 13:34:56'),
(216, 'ahmed-maiwada', 'blank.png', '2', '1', '20', '', 'Birnin-Gwari/Giwa', 'Ahmed Maiwada', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:35:29', '2015-06-14 13:35:29'),
(217, 'aliyu-inuwam.', 'blank.png', '2', '1', '20', '', 'Lere', 'Aliyu InuwaM.', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:35:58', '2015-06-14 13:35:58'),
(218, 'asake-jonathan', 'blank.png', '2', '1', '20', '', 'Zangon Kataf/Jaba', 'Asake Jonathan', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:36:27', '2015-06-14 13:36:27'),
(219, 'audu-ado-dogo', 'blank.png', '2', '1', '20', '', 'Jema''a/Sanga', 'Audu Ado Dogo', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:36:52', '2015-06-14 13:36:52'),
(220, 'aya-florence-diya', 'blank.png', '2', '1', '20', '', 'Kaura', 'Aya Florence Diya', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:37:21', '2015-06-14 13:37:21'),
(221, 'dansa-audu', 'blank.png', '2', '1', '20', '', 'Kauru', 'Dansa Audu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:37:54', '2015-06-14 13:37:54'),
(222, 'haruna-mohammed-mikalu', 'blank.png', '2', '1', '20', '', 'Igabi', 'Haruna Mohammed Mikalu', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:38:16', '2015-06-14 13:38:16'),
(223, 'isiaku-yakubu-m.', 'blank.png', '2', '1', '20', '', 'Chikun/Kajuru', 'Isiaku Yakubu M.', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:38:52', '2015-06-14 13:38:52'),
(224, 'jagaba-adams', 'blank.png', '2', '1', '20', '', 'Kachia/Kagarko', 'Jagaba Adams', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:40:46', '2015-06-14 13:40:46'),
(225, 'koji-binta-garba', 'blank.png', '2', '1', '20', '', 'Kaduna South', 'Koji Binta Garba', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:41:09', '2015-06-14 13:41:09'),
(226, 'liiloro-mohammed-hussain', 'blank.png', '2', '1', '20', '', 'Makarfi/Kudan', 'Liiloro Mohammed Hussain', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:41:55', '2015-06-14 13:41:55'),
(227, 'paki-tijjani-sani', 'blank.png', '2', '1', '20', '', 'Ikara/Kubau', 'Paki Tijjani Sani', 'PDP', 1999, 2003, 1, '1', '0', 'yes', '2015-06-14 13:42:25', '2015-06-14 13:42:25'),
(228, 'tanko-yahaya', 'blank.png', '2', '1', '20', '', 'Kaduna North', 'Tanko Yahaya', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:42:56', '2015-06-14 13:42:56'),
(229, 'tukur-abdul''rauf', 'blank.png', '2', '1', '20', '', 'Soba', 'Tukur Abdul''Rauf', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:43:21', '2015-06-14 13:43:21'),
(230, 'usman-abdukadir', 'blank.png', '2', '1', '20', '', 'Zaria Federal', 'Usman Abdukadir', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:43:50', '2015-06-14 13:43:50'),
(231, 'abdullahi-musa-nuhu', 'blank.png', '2', '1', '22', '', 'Kaita/Jibia', 'Abdullahi Musa Nuhu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:44:16', '2015-06-14 13:44:16'),
(232, 'aminu-bello-musari', 'blank.png', '2', '1', '22', '', 'Malum Fashi/Kafur', 'Aminu Bello Musari', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:44:47', '2015-06-14 13:44:47'),
(233, 'daura-adamusaidu', 'blank.png', '2', '1', '22', '', 'Daura/Sandamu/Mai''Adua', 'Daura AdamuSaidu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:45:22', '2015-06-14 13:45:22'),
(234, 'funtua-lawal-ibrahim', 'blank.png', '2', '1', '22', '', 'Funtua/Dandume', 'Funtua Lawal Ibrahim', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:45:52', '2015-06-14 13:45:52'),
(235, 'funtua-lawal-ibrahim', 'blank.png', '2', '1', '22', '', 'Funtua/Dandume', 'Funtua Lawal Ibrahim', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:45:56', '2015-06-14 13:45:56'),
(236, 'makera-sabiu-hassan', 'blank.png', '2', '1', '22', '', 'Dutsin-ma/Kurfi', 'Makera Sabiu Hassan', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:46:27', '2015-06-14 13:46:27'),
(237, 'mashi-abdu-haro', 'blank.png', '2', '1', '22', '', 'Mashi/Dvisi', 'Mashi Abdu Haro', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:46:52', '2015-06-14 13:46:52'),
(238, 'musa-aliyu', 'blank.png', '2', '1', '22', '', 'Mani/Bindawa', 'Musa Aliyu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:47:21', '2015-06-14 13:47:21'),
(239, 'nadabo-tukur-idris', 'blank.png', '2', '1', '22', '', 'Bakori/Danja', 'Nadabo Tukur Idris', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:47:51', '2015-06-14 13:47:51'),
(240, 'nasarawa-usman-mani', 'blank.png', '2', '1', '22', '', 'Kankia/Ingawa/Kusada', 'Nasarawa Usman Mani', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:48:14', '2015-06-14 13:48:14'),
(241, 'safana-amina-yakubu', 'blank.png', '2', '1', '22', '', 'Safana/Batsari/Dan-Musa', 'Safana Amina Yakubu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:48:54', '2015-06-14 13:48:54'),
(242, 'shehu-abubakar-garba', 'blank.png', '2', '1', '22', '', 'Musawa/Matazu', 'Shehu Abubakar Garba', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:49:34', '2015-06-14 13:49:34'),
(243, 'tsagero-muazu-lemamu', 'blank.png', '2', '1', '22', '', 'Rimi/Charanchi/Batagarawa', 'Tsagero Muazu Lemamu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:49:56', '2015-06-14 13:49:56'),
(244, 'yahaya-shuaib-baure', 'blank.png', '2', '1', '22', '', 'Baure/Zango', 'Yahaya Shuaib Baure', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:50:21', '2015-06-14 13:50:21'),
(245, 'yankara-lawal-yusufu', 'blank.png', '2', '1', '22', '', 'Kankara/Sabuwa/Faskari', 'Yankara Lawal Yusufu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:50:45', '2015-06-14 13:50:45'),
(246, 'yar''adua-abubakar-sadiq', 'blank.png', '2', '1', '22', '', 'Katsina Central', 'Yar''adua Abubakar Sadiq', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:51:07', '2015-06-14 13:51:07'),
(247, 'abubakar-lamido-sadiq', 'blank.png', '2', '1', '22', '', 'Katsina North Central', 'Abubakar Lamido Sadiq', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:51:31', '2015-06-14 13:51:31'),
(248, 'abdulkareem-saliu', 'blank.png', '2', '1', '24', '', 'Adavi/Okehi', 'Abdulkareem Saliu', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:51:55', '2015-06-14 13:51:55'),
(249, 'abiodun-ojo-samuel', 'blank.png', '2', '1', '24', '', 'Ijumu/Kabba-Bunu', 'Abiodun Ojo Samuel', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:52:16', '2015-06-14 13:52:16'),
(250, 'ineke-itaka-frank', 'blank.png', '2', '1', '24', '', 'Idah/Ofu/Ibaji/Igala-Lamela-Odolu', 'Ineke Itaka Frank', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:52:41', '2015-06-14 13:52:41'),
(251, 'jimba-isaac-ruzama', 'blank.png', '2', '1', '24', '', 'Bassa/Dekina', 'Jimba Isaac Ruzama', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:53:05', '2015-06-14 13:53:05'),
(252, 'muazu-abimaje', 'blank.png', '2', '1', '24', '', 'Ankpa Olamaboro Omala', 'Muazu Abimaje', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:53:33', '2015-06-14 13:53:33'),
(253, 'musa-adamu-osuku', 'blank.png', '2', '1', '24', '', 'Lokoja/Kogi/KK', 'Musa Adamu Osuku', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:53:58', '2015-06-14 13:53:58'),
(254, 'olorunshola-ojo', 'blank.png', '2', '1', '24', '', 'Yagba East/West/Modamuko', 'Olorunshola Ojo', 'Kogi', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:54:29', '2015-06-14 13:54:29'),
(255, 'samari-adamu-ahmadu', 'blank.png', '2', '1', '24', '', 'Ajaokuta', 'Samari Adamu Ahmadu', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:55:01', '2015-06-14 13:55:01'),
(256, 'sani-abdul-stephen-e.', 'blank.png', '2', '1', '24', '', 'Okene/Ogori/Magongo', 'Sani Abdul Stephen E.', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:55:24', '2015-06-14 13:55:24'),
(257, 'isa-ibrahimbio', 'blank.png', '2', '1', '25', '', 'Baruten/Kaiama', 'Isa IbrahimBio', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:55:50', '2015-06-14 13:55:50'),
(258, 'bola-oni-bashir', 'blank.png', '2', '1', '25', '', 'Ekiti/Isin/Irepodun/Oke-ero', 'Bola Oni Bashir', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:56:17', '2015-06-14 13:56:17'),
(259, 'saraki-fowora-rukayat-gbemisola', 'blank.png', '2', '1', '25', '', 'Asa/Ilorin West', 'Saraki-Fowora Rukayat Gbemisola', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:56:41', '2015-06-14 13:56:41'),
(260, 'sarouk-farouk-abdul-wahab', 'blank.png', '2', '1', '25', '', 'Ilorin East/South', 'Sarouk Farouk Abdul Wahab', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:57:02', '2015-06-14 13:57:02'),
(261, 'shittu-rauf-kolawole', 'blank.png', '2', '1', '25', '', 'Offa/Oyun/Ifelodun', 'Shittu Rauf Kolawole', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:57:26', '2015-06-14 13:57:26'),
(262, 'yinusa-yahaya-ahmed', 'blank.png', '2', '1', '25', '', 'Edu/Moro/Patigi', 'Yinusa Yahaya Ahmed', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:58:12', '2015-06-14 13:58:12'),
(263, 'aguebor-sunday-ikponmwonsa', 'blank.png', '2', '1', '14', '', 'Egor/Ikpoba-okha', 'Aguebor Sunday Ikponmwonsa', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:58:34', '2015-06-14 13:58:34'),
(264, 'alegbe-benson', 'blank.png', '2', '1', '14', '', 'Owan West/East', 'Alegbe Benson', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:58:57', '2015-06-14 13:58:57'),
(265, 'aziegbemi-anthony', 'blank.png', '2', '1', '14', '', 'Esan North-East/Esan South- East', 'Aziegbemi Anthony', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 13:59:27', '2015-06-14 13:59:27'),
(266, 'ehimen-oiboh-gabriel', 'blank.png', '2', '1', '14', '', 'Esan Central/West/Igueben', 'Ehimen Oiboh Gabriel', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:00:29', '2015-06-14 14:00:29'),
(267, 'idahosa-ehiogiewest', 'blank.png', '2', '1', '14', '', 'Ovia South/West-Ovia North/East', 'Idahosa EhiogieWest', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:01:20', '2015-06-14 14:01:20'),
(268, 'lakoju-joseph-babatunde', 'blank.png', '2', '1', '14', '', 'Akoko-Edo', 'Lakoju Joseph Babatunde', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:02:12', '2015-06-14 14:02:12'),
(269, 'augustine-u.-obozuwa', 'blank.png', '2', '1', '14', '', 'Etsako East/West/Central', 'Augustine U. Obozuwa', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:03:23', '2015-06-14 14:03:23'),
(270, 'odubu-egberanmwen-pius', 'blank.png', '2', '1', '14', '', 'Orhionmwon/Uhunmwode', 'Odubu Egberanmwen Pius', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:03:46', '2015-06-14 14:03:46'),
(271, 'ogbiede-enoma-willie', 'blank.png', '2', '1', '14', '', 'Oredo', 'Ogbiede Enoma Willie', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:04:56', '2015-06-14 14:04:56'),
(272, 'orimoloye-peter-saka', 'blank.png', '2', '1', '31', '', 'Akoko South East/South West', 'Orimoloye Peter Saka', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:09:49', '2015-06-14 14:09:49'),
(273, 'abayomi-sheba', 'blank.png', '2', '1', '31', '', 'Irele/Okitipupa', 'Abayomi Sheba', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:11:13', '2015-06-14 14:11:13'),
(274, 'ayo-adeyemi-janet-f.', 'blank.png', '2', '1', '31', '', 'Ile-Oluji Okeigbo/Odigbo', 'Ayo-Adeyemi Janet F.', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:11:39', '2015-06-14 14:11:39'),
(275, 'dada-busari', 'blank.png', '2', '1', '31', '', 'Akoko North East/north West', 'Dada Busari', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:12:06', '2015-06-14 14:12:06'),
(276, 'fagbite-lawrence-akinyele', 'blank.png', '2', '1', '31', '', 'Idanre/Ifedore', 'Fagbite Lawrence Akinyele', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:12:29', '2015-06-14 14:12:29'),
(277, 'oladejo-akinnifesi', 'blank.png', '2', '1', '31', '', 'Ondo East/ West', 'Oladejo Akinnifesi', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:12:53', '2015-06-14 14:12:53'),
(278, 'omoseebi-rufus-akinwumi', 'blank.png', '2', '1', '31', '', 'Akure North /South', 'Omoseebi Rufus Akinwumi', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:13:22', '2015-06-14 14:13:22'),
(279, 'omotosho-michael', 'blank.png', '2', '1', '31', '', 'Owo/Ose', 'Omotosho Michael', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:13:46', '2015-06-14 14:13:46'),
(280, 'adekunle-jenrade-kareem', 'blank.png', '2', '1', '33', '', 'Ona-Ara/Egbeda', 'Adekunle Jenrade Kareem', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:14:17', '2015-06-14 14:14:17'),
(281, 'ademola-adeniyi-tajudeen', 'blank.png', '2', '1', '33', '', 'Iseyin/Kajola/Iwujola/Itesiwaju', 'Ademola Adeniyi Tajudeen', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:14:39', '2015-06-14 14:14:39'),
(282, 'adeshile-adedeji-kazeem', 'blank.png', '2', '1', '33', '', 'Ibadan North- East/South- East', 'Adeshile Adedeji Kazeem', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:15:09', '2015-06-14 14:15:09'),
(283, 'alli-lateef-adebola', 'blank.png', '2', '1', '33', '', 'Lagelu/Akinyele', 'Alli Lateef Adebola', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:15:29', '2015-06-14 14:15:29');
INSERT INTO `appointments` (`id`, `slug`, `avatar`, `position_id`, `country_id`, `state`, `capital`, `region`, `full_name`, `party`, `term_a`, `term_b`, `month`, `office_time`, `term`, `elect`, `created_at`, `updated_at`) VALUES
(284, 'anwo-sanusisadiq', 'blank.png', '2', '1', '33', '', 'Ibarapa East/Ido', 'Anwo SanusiSadiq', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:15:54', '2015-06-14 14:15:54'),
(285, 'balogun-alli', 'blank.png', '2', '1', '33', '', 'Ibadan North', 'Balogun Alli', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:16:22', '2015-06-14 14:16:22'),
(286, 'gbadamosi-lateef-durojaiye', 'blank.png', '2', '1', '33', '', 'Oluyole Local Govt.', 'Gbadamosi Lateef Durojaiye', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:16:42', '2015-06-14 14:16:42'),
(287, 'hussein-adesiyan-ajani', 'blank.png', '2', '1', '33', '', 'Atisbo/Saki East/Saki West', 'Hussein Adesiyan Ajani', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:17:04', '2015-06-14 14:17:04'),
(288, 'oduyoye-babatunde', 'blank.png', '2', '1', '33', '', 'Ibadan North West/South West', 'Oduyoye Babatunde', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:17:27', '2015-06-14 14:17:27'),
(289, 'oduyoye-babatunde', 'blank.png', '2', '1', '33', '', 'Ibadan North West/South West', 'Oduyoye Babatunde', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:17:33', '2015-06-14 14:17:33'),
(290, 'oladeni-ojegokerafiu', 'blank.png', '2', '1', '33', '', 'Ibarapa Central/Ibarapa North', 'Oladeni OjegokeRafiu', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:22:46', '2015-06-14 14:22:46'),
(291, 'oluwasegun-taiwo-dele', 'blank.png', '2', '1', '33', '', 'Oyo Alafin', 'Oluwasegun Taiwo Dele', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:23:08', '2015-06-14 14:23:08'),
(292, 'oyebanji-folaranmi-simeon', 'blank.png', '2', '1', '33', '', 'Ogo-oluwa', 'Oyebanji Folaranmi Simeon', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:23:45', '2015-06-14 14:23:45'),
(293, 'segun-adibitemilola', 'blank.png', '2', '1', '33', '', 'Ogbomosho/North/South/Orire', 'Segun AdibiTemilola', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:24:11', '2015-06-14 14:24:11'),
(294, 'sekoni-sheik-mudathir', 'blank.png', '2', '1', '33', '', 'Olurunsogo/Dorelope', 'Sekoni Sheik mudathir', 'AD', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:24:41', '2015-06-14 14:24:41'),
(295, 'binuwai-josiah-gobum', 'blank.png', '2', '1', '34', '', 'Pankshin/Kanke/Kanam', 'Binuwai Josiah Gobum', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:25:04', '2015-06-14 14:25:04'),
(296, 'adeh-lumumba-dah', 'blank.png', '2', '1', '34', '', 'Bassa/Jos North', 'Adeh Lumumba Dah', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:25:35', '2015-06-14 14:25:35'),
(297, 'damulak-jafaru-muhaammadu', 'blank.png', '2', '1', '34', '', 'Mikang/Quan.Pan/Shendam', 'Damulak Jafaru Muhaammadu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:26:03', '2015-06-14 14:26:03'),
(298, 'dung-daniel-sunday', 'blank.png', '2', '1', '34', '', 'JosSouth/West', 'Dung Daniel Sunday', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:26:32', '2015-06-14 14:26:32'),
(299, 'lar-victor-rampyal', 'blank.png', '2', '1', '34', '', 'Barkin Ladi/Riyom', 'Lar Victor Rampyal', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:27:01', '2015-06-14 14:27:01'),
(300, 'yero-ibrahim-bello', 'blank.png', '2', '1', '34', '', 'Wase', 'Yero Ibrahim Bello', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:27:25', '2015-06-14 14:27:25'),
(301, 'yilkes-barminasali', 'blank.png', '2', '1', '34', '', 'Mangu/Bokkos', 'Yilkes BarminasAli', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:27:56', '2015-06-14 14:27:56'),
(302, 'longjohn-tonye-tamuno', 'blank.png', '2', '1', '35', '', 'Bonny/Degema', 'Longjohn Tonye Tamuno', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:28:23', '2015-06-14 14:28:23'),
(303, 'abibo-promise-iderifama-t.', 'blank.png', '2', '1', '35', '', 'Okrika/Ogu-bolo', 'Abibo Promise Iderifama T.', 'ANPP', 1999, 2003, 1, '1', '0', 'yes', '2015-06-14 14:28:47', '2015-06-14 14:28:47'),
(304, 'adokiye-young-harry', 'blank.png', '2', '1', '35', '', 'Asalga/Akulga', 'Adokiye Young-Harry', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:29:09', '2015-06-14 14:29:09'),
(305, 'wilson-asinobi-ake', 'blank.png', '2', '1', '35', '', 'Ahoada-West/Ogba-Egbema-Ndoni', 'Wilson Asinobi Ake', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:29:30', '2015-06-14 14:29:30'),
(306, 'amadi-lasbry-onusegbu', 'blank.png', '2', '1', '35', '', 'Port Harcourt I', 'Amadi Lasbry Onusegbu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:29:52', '2015-06-14 14:29:52'),
(307, 'amadi-appolos-e.', 'blank.png', '2', '1', '35', '', 'Ikwerre/Emohua', 'Amadi Appolos E.', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:30:11', '2015-06-14 14:30:11'),
(308, 'barida-mikko-bernard', 'blank.png', '2', '1', '35', '', 'Khana/Gokana', 'Barida Mikko Bernard', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:30:31', '2015-06-14 14:30:31'),
(309, 'nwala-iwezor-jacob', 'blank.png', '2', '1', '35', '', 'Etche/Omuma', 'Nwala Iwezor Jacob', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:30:54', '2015-06-14 14:30:54'),
(310, 'olaka-nwogu', 'blank.png', '2', '1', '35', '', 'Eleme/Tai/Oyigbo', 'Olaka Nwogu', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:31:20', '2015-06-14 14:31:20'),
(311, 'chibudom-nwuche', 'blank.png', '2', '1', '35', '', 'Ahoada-East;Abua/Odual', 'Chibudom Nwuche', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:32:03', '2015-06-14 14:32:03'),
(312, 'ogonda-oludi-edwin', 'blank.png', '2', '1', '35', '', 'Obio/Akpor', 'Ogonda Oludi Edwin', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:32:24', '2015-06-14 14:32:24'),
(313, 'opara-austin-adiele', 'blank.png', '2', '1', '35', '', 'Port Harcourt II', 'Opara Austin Adiele', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:32:50', '2015-06-14 14:32:50'),
(314, 'owor-jeffreys-m.', 'blank.png', '2', '1', '35', '', 'Andoni/Opobo/Nkoro', 'Owor Jeffreys M.', 'PDP', 1999, 2003, 1, '1', '0', 'yes', '2015-06-14 14:33:21', '2015-06-14 14:33:21'),
(315, 'babale-jibrin', 'blank.png', '2', '1', '38', '', 'Gulani/Gujba/Damaturu/Tarmuwa', 'Babale Jibrin', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:33:43', '2015-06-14 14:33:43'),
(316, 'geidam-almajir', 'blank.png', '2', '1', '38', '', 'Bursari/Geidam/Yunusari', 'Geidam Almajir', 'ANPP', 1999, 2003, 1, '1', '0', 'yes', '2015-06-14 14:34:09', '2015-06-14 14:34:09'),
(317, 'isiyaku-suleiman', 'blank.png', '2', '1', '38', '', 'Fika/Fune', 'Isiyaku Suleiman', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:34:31', '2015-06-14 14:34:31'),
(318, 'jonga-hassan', 'blank.png', '2', '1', '38', '', 'Nangere/Pootiskum', 'Jonga Hassan', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:34:49', '2015-06-14 14:34:49'),
(319, 'lawan-ahmadibrahim', 'blank.png', '2', '1', '38', '', 'Bade/Jakusko', 'Lawan AhmadIbrahim', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:35:09', '2015-06-14 14:35:09'),
(320, 'shugaba-bello', 'blank.png', '2', '1', '38', '', 'Yusufari/Nguru/Machina/Karasuwa', 'Shugaba Bello', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:35:33', '2015-06-14 14:35:33'),
(321, 'abubakar-makwashi', 'blank.png', '2', '1', '37', '', 'Bakura/Maradun', 'Abubakar Makwashi', 'ANPP', 1999, 2003, 1, '1', '0', 'yes', '2015-06-14 14:36:03', '2015-06-14 14:36:03'),
(322, 'aliyu-sahabi', 'blank.png', '2', '1', '37', '', 'Gummi/Bukkuyum', 'Aliyu Sahabi', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:36:25', '2015-06-14 14:36:25'),
(323, 'doruwa-sani-ibrahim-r.', 'blank.png', '2', '1', '37', '', 'Bungudu/Maru', 'Doruwa Sani Ibrahim R.', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:37:46', '2015-06-14 14:37:46'),
(324, 'godal-lawali-ibrahim-nasarawa', 'blank.png', '2', '1', '37', '', 'Kaura Namoda/Birnin Magaji', 'Godal Lawali Ibrahim Nasarawa', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:39:02', '2015-06-14 14:39:02'),
(325, 'mohammed-sani', 'blank.png', '2', '1', '37', '', 'Tsafe/Gusau', 'Mohammed Sani', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:39:31', '2015-06-14 14:39:31'),
(326, 'moriki-bello-abubakar', 'blank.png', '2', '1', '37', '', 'Zurmi/Shinkafi', 'Moriki Bello Abubakar', 'ANPP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:40:10', '2015-06-14 14:40:10'),
(327, 'sani-anka-mohammed', 'blank.png', '2', '1', '37', '', 'Anka/Mafara', 'Sani Anka Mohammed', 'PDP', 1999, 2003, 1, '1', '0', NULL, '2015-06-14 14:40:39', '2015-06-14 14:40:39'),
(328, 'salau-tunde', 'blank.png', '2', '1', '26', '', 'Epe', 'Salau Tunde', 'PDP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:17:45', '2015-06-15 15:17:45'),
(329, 'setonji-g.-koshoedo', 'blank.png', '2', '1', '26', '', 'Badagry', 'Setonji G. Koshoedo', 'PDP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:18:46', '2015-06-15 15:18:46'),
(330, 'amele-moshood-tokunbo', 'blank.png', '2', '1', '26', '', 'Ibeju-Lekki', 'Amele Moshood Tokunbo', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:19:38', '2015-06-15 15:19:38'),
(331, 'habeeb-adekunle-fashiro', 'blank.png', '2', '1', '26', '', 'Eti Osa', 'Habeeb Adekunle Fashiro', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:20:10', '2015-06-15 15:20:10'),
(332, 'ganiyu-oladunjoye-hamzat', 'blank.png', '2', '1', '26', '', 'Mushin II', 'Ganiyu Oladunjoye Hamzat', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:20:39', '2015-06-15 15:20:39'),
(333, 'abiola-edewor', 'blank.png', '2', '1', '26', '', 'Apapa', 'Abiola Edewor', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:21:24', '2015-06-15 15:21:24'),
(334, 'adeyemi-olwole', 'blank.png', '2', '1', '26', '', 'Somolu', 'Adeyemi Olwole', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:22:08', '2015-06-15 15:22:08'),
(335, 'fancy-akeem-arole', 'blank.png', '2', '1', '26', '', 'Surulere II', 'Fancy Akeem Arole', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:22:38', '2015-06-15 15:22:38'),
(336, 'femi-obayomi-davies', 'blank.png', '2', '1', '26', '', 'Ojo', 'Femi Obayomi Davies', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:23:07', '2015-06-15 15:23:07'),
(337, 'adeyanju-simon-aderemi', 'blank.png', '2', '1', '26', '', 'Amunwo', 'Adeyanju Simon Aderemi', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:23:33', '2015-06-15 15:23:33'),
(338, 'tugbobo-aima', 'blank.png', '2', '1', '26', '', 'Ikeja', 'Tugbobo Aima', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:24:06', '2015-06-15 15:24:06'),
(339, 'femi-gbaja-biamila', 'blank.png', '2', '1', '26', '', 'Surulere I', 'Femi Gbaja Biamila', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:24:40', '2015-06-15 15:24:40'),
(340, 'abike-dabiri', 'blank.png', '2', '1', '26', '', 'Ikorodu', 'Abike Dabiri', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:25:07', '2015-06-15 15:25:07'),
(341, 'olajumoke-okoya-thomas', 'blank.png', '2', '1', '26', '', 'Lagos Island', 'Olajumoke Okoya-Thomas', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:26:06', '2015-06-15 15:26:06'),
(342, 'olakunle-amunkoro', 'blank.png', '2', '1', '26', '', 'Agege', 'Olakunle Amunkoro', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:26:41', '2015-06-15 15:26:41'),
(343, 'ganiyu-olarenwaju-solomon', 'blank.png', '2', '1', '26', '', 'Mushin I', 'Ganiyu Olarenwaju Solomon', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:28:21', '2015-06-15 15:28:21'),
(344, 'monsuru-alao-owolabi', 'blank.png', '2', '1', '26', '', 'Lagos Island II', 'Monsuru Alao Owolabi', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:28:50', '2015-06-15 15:28:50'),
(345, 'onimole-olufemi-olawale', 'blank.png', '2', '1', '26', '', 'Ifako-Ijaiye', 'Onimole Olufemi Olawale', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:29:21', '2015-06-15 15:29:21'),
(346, 'joseph-jaiyeola-ajatta', 'blank.png', '2', '1', '26', '', 'Oshodi-Isolo II', 'Joseph Jaiyeola Ajatta', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:29:58', '2015-06-15 15:29:58'),
(347, 'mudashiru-oyetunde-husaini', 'blank.png', '2', '1', '26', '', 'Oshodi-Isolo I', 'Mudashiru Oyetunde Husaini', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:30:23', '2015-06-15 15:30:23'),
(348, 'ogunbajo-olu', 'blank.png', '2', '1', '26', '', 'Ajeromi/Ifelodun', 'Ogunbajo Olu', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:30:53', '2015-06-15 15:30:53'),
(349, 'wunmi-bewaji-mabayoje', 'blank.png', '2', '1', '26', '', 'Lagos Mainland', 'Wunmi Bewaji Mabayoje', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:31:30', '2015-06-15 15:31:30'),
(350, 'oludotun-animashaun', 'blank.png', '2', '1', '26', '', 'Kosofe', 'Oludotun Animashaun', 'AD', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:32:06', '2015-06-15 15:32:06'),
(351, 'sodienye-swerve', 'blank.png', '2', '1', '', '', 'Okrika/Ogu-bolo', 'sodienye swerve', '', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:32:53', '2015-06-15 15:32:53'),
(352, 'kunaiyi-akpanah-daemi', 'blank.png', '2', '1', '', '', 'Asalga/Akulga', 'Kunaiyi-Akpanah Daemi', 'PDP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:33:34', '2015-06-15 15:33:34'),
(353, 'aguma-igochukwu', 'blank.png', '2', '1', '', '', 'Port Harcourt I', 'Aguma Igochukwu', 'PDP', 1993, 2007, 1, '1', '0', NULL, '2015-06-15 15:34:18', '2015-06-15 15:34:18'),
(354, 'uchendu-andrew-i.', 'blank.png', '2', '1', '', '', 'Ikwerre/Emohua', 'Uchendu Andrew I.', 'PDP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:34:55', '2015-06-15 15:34:55'),
(355, 'deeyah-emmanuel-nwika', 'blank.png', '2', '1', '', '', 'Khana/Gokana', 'Deeyah Emmanuel Nwika', 'PDP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:35:19', '2015-06-15 15:35:19'),
(356, 'nwosu-george-ford', 'blank.png', '2', '1', '', '', 'Etche/Omuma', 'Nwosu George Ford', 'PDP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:35:48', '2015-06-15 15:35:48'),
(357, 'olaka-nwogu', 'blank.png', '2', '1', '', '', 'Eleme/Tai/Oyigbo', 'Olaka Nwogu', 'PDP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:36:13', '2015-06-15 15:36:13'),
(358, 'olaka-nwogu', 'blank.png', '2', '1', '', '', 'Eleme/Tai/Oyigbo', 'Olaka Nwogu', 'PDP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:36:14', '2015-06-15 15:36:14'),
(359, 'osinakachukwu-ideozu', 'blank.png', '2', '1', '', '', 'Ahoada-East;Abua/Odual', 'Osinakachukwu Ideozu', 'PDP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:36:44', '2015-06-15 15:36:44'),
(360, 'chinwo-ike-b.-s', 'blank.png', '2', '1', '', '', 'Obio/Akpor', 'Chinwo Ike B. S', 'PDP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:37:09', '2015-06-15 15:37:09'),
(361, 'owor-jeffreys-m', 'blank.png', '2', '1', '', '', 'Andoni/Opobo/Nkoro', 'Owor Jeffreys M', 'PDP', 2003, 2007, 1, '1', '0', 'yes', '2015-06-15 15:37:40', '2015-06-15 15:37:40'),
(362, 'ado-sidi', 'blank.png', '2', '1', '9', '', 'Abuja-South', 'Ado Sidi', 'ANPP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:38:08', '2015-06-15 15:38:08'),
(363, 'philip-tanimu-aduda', 'blank.png', '2', '1', '9', '', 'AMAC/Bwari', 'Philip Tanimu Aduda', 'PDP', 2003, 2007, 1, '1', '0', NULL, '2015-06-15 15:38:44', '2015-06-15 15:38:44'),
(364, 'chris-ngige', 'blank.png', '1', '1', '1', 'Akwa', 'South-East', 'Chris Ngige', 'PDP', 2003, 2007, 5, '3', '1', 'yes', '2015-06-16 10:31:10', '2015-06-16 10:31:10'),
(365, 'chimaroke-nnamani', 'blank.png', '1', '1', '2', 'Enugu', 'South-East', 'Chimaroke Nnamani', 'PDP', 1999, 2003, 5, '4', '1', 'yes', '2015-06-16 10:31:56', '2015-06-16 10:31:56'),
(366, 'victor-attah', 'blank.png', '1', '1', '3', 'Uyo', 'South-South', 'Victor Attah', 'PDP', 1999, 2003, 5, '4', '1', 'yes', '2015-06-16 10:33:20', '2015-06-16 10:33:20'),
(367, 'boni-haruna', 'blank.png', '1', '1', '4', 'Yola', 'North-East', 'Boni Haruna', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-16 10:38:32', '2015-06-16 10:38:32'),
(368, 'orji-uzor-kalu', 'blank.png', '1', '1', '5', 'Umuahia', 'South-East', 'Orji Uzor Kalu', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-16 10:42:52', '2015-06-16 10:42:52'),
(369, 'adamu-mu''azu', 'blank.png', '1', '1', '6', 'Bauchi', 'North-East', 'Adamu Mu''azu', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-16 10:44:38', '2015-06-16 10:44:38'),
(370, 'diepreye-alamieyeseigha', 'blank.png', '1', '1', '7', 'Yenagoa', 'South-South', 'Diepreye Alamieyeseigha', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-16 10:45:19', '2015-06-16 10:45:19'),
(371, 'ali-modu-sheriff', 'blank.png', '1', '1', '10', 'Maiduguri', 'North-East', 'Ali Modu Sheriff', 'PDP', 2003, 2007, 5, '4', '2', 'yes', '2015-06-16 10:57:45', '2015-06-16 10:57:45'),
(372, 'chief-lucky-igbinedion', 'blank.png', '1', '1', '14', 'Benin-City', 'South-South', 'Chief Lucky Igbinedion', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-16 11:02:11', '2015-06-16 11:02:11'),
(373, 'ayo-fayose', 'blank.png', '1', '1', '15', 'Ado Ekiti', 'South-West', 'Ayo Fayose', 'PDP', 2003, 0, 5, '3', '1', 'yes', '2015-06-16 11:03:09', '2015-06-16 11:03:09'),
(374, 'chief-friday-aderemi', 'blank.png', '1', '1', '15', 'Ado Ekiti', 'South-West', 'Chief Friday Aderemi', 'PDP', 2006, 0, 5, '1', '0', 'no', '2015-06-16 11:03:59', '2015-06-16 11:03:59'),
(375, 'tunji-olurin', 'blank.png', '1', '1', '15', 'Ado Ekiti', 'South-West', 'Tunji Olurin', 'PDP', 2006, 0, 10, '1', '1', 'no', '2015-06-16 11:04:50', '2015-06-16 11:04:50'),
(376, 'tope-ademiluyi', 'blank.png', '1', '1', '15', 'Ado Ekiti', 'South-West', 'Tope Ademiluyi', 'PDP', 2007, 0, 4, '1', '1', 'no', '2015-06-16 11:06:33', '2015-06-16 11:06:33'),
(377, 'segun-oni', 'blank.png', '1', '1', '15', 'Ado Ekiti', 'South-West', 'Segun Oni', 'PDP', 2007, 0, 5, '3', '1', 'yes', '2015-06-16 11:07:27', '2015-06-16 11:07:27'),
(378, 'mohammed-danjuma-goje', 'blank.png', '1', '1', '17', 'Gombe', 'North-East', 'Mohammed Danjuma Goje', 'PDP', 2003, 0, 5, '4', '1', 'yes', '2015-06-16 11:08:12', '2015-06-16 11:08:12'),
(379, 'achike-udenwa', 'blank.png', '1', '1', '18', 'Owerri', 'South-South', 'Achike Udenwa', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-16 11:09:02', '2015-06-16 11:09:02'),
(380, 'ibrahim-saminu-turaki', 'blank.png', '1', '1', '19', 'Dutse', 'North-Central', 'Ibrahim Saminu Turaki', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-16 11:09:53', '2015-06-16 11:09:53'),
(381, 'ahmed-makarfi', 'blank.png', '0', '1', '20', 'Kaduna', 'North-West', 'Ahmed Makarfi', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-16 12:27:50', '2015-06-16 12:27:50'),
(382, 'umaru-musa-yar''adua', 'blank.png', '1', '1', '22', 'Katsina', 'North', 'Umaru Musa Yar''Adua', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-17 10:11:09', '2015-06-17 10:11:09'),
(383, 'adamu-aliero', 'blank.png', '1', '1', '23', 'Birnin Kebbi', 'North-West', 'Adamu Aliero', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-17 10:12:02', '2015-06-17 10:12:02'),
(384, 'ibrahim-idris', 'blank.png', '1', '1', '24', 'Lokoja', 'Central', 'Ibrahim Idris', 'PDP', 2003, 0, 5, '4', '2', 'yes', '2015-06-17 10:14:26', '2015-06-17 10:14:26'),
(385, 'bukola-saraki', 'blank.png', '1', '1', '25', 'Illorin', 'West', 'Bukola Saraki', 'PDP', 2003, 0, 5, '4', '1', 'yes', '2015-06-17 10:15:02', '2015-06-17 10:15:02'),
(386, 'mr-bola-tinubu[', 'blank.png', '1', '1', '26', 'Ikeja', 'South-West', 'Mr Bola Tinubu[', 'PDP', 1999, 0, 5, '4', '2', 'yes', '2015-06-17 10:16:16', '2015-06-17 10:16:16'),
(387, 'abdullahi-adamu', 'blank.png', '1', '1', '27', 'Lafia', 'Central', 'Abdullahi Adamu', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-17 10:24:04', '2015-06-17 10:24:04'),
(388, 'abdulkadir-kure', 'blank.png', '1', '1', '28', 'Minna', 'North-West', 'Abdulkadir Kure', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-17 10:24:44', '2015-06-17 10:24:44'),
(389, 'gbenga-daniel', 'blank.png', '1', '1', '29', 'Abeokuta', 'South-West', 'Gbenga Daniel', 'PDP', 2003, 0, 5, '4', '1', 'yes', '2015-06-17 10:25:35', '2015-06-17 10:25:35'),
(390, 'olusegun-agagu', 'blank.png', '1', '1', '30', 'Akure', 'South-West', 'Olusegun Agagu', 'PDP', 2003, 0, 5, '4', '1', 'yes', '2015-06-17 10:26:22', '2015-06-17 10:26:22'),
(391, 'olagunsoye-oyinlola', 'blank.png', '1', '1', '31', 'Osogbo', 'South-West', 'Olagunsoye Oyinlola', 'PDP', 2003, 0, 5, '4', '1', 'yes', '2015-06-17 10:26:56', '2015-06-17 10:26:56'),
(392, 'rashidi-adewolu-ladoja', 'blank.png', '1', '1', '32', 'Ibadan', 'South-West', 'Rashidi Adewolu Ladoja', 'PDP', 2003, 0, 5, '4', '1', 'yes', '2015-06-17 10:27:34', '2015-06-17 10:27:34'),
(393, 'peter-odili', 'blank.png', '1', '1', '34', 'Port Harcourt', 'South-South', 'Peter Odili', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-17 10:28:39', '2015-06-17 10:28:39'),
(394, 'attahiru-bafarawa', 'blank.png', '1', '1', '35', 'Sokoto', 'North-West', 'Attahiru Bafarawa', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-17 10:29:17', '2015-06-17 10:29:17'),
(395, 'jolly-nyame', 'blank.png', '1', '1', '36', 'Jalingo', 'East', 'Jolly Nyame', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-17 10:29:48', '2015-06-17 10:29:48'),
(396, 'bukar-abba-ibrahim', 'blank.png', '1', '1', '38', 'Damaturu', 'North-East', 'Bukar Abba Ibrahim', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-17 10:30:26', '2015-06-17 10:30:26'),
(397, 'ahmad-sani-yerima', 'blank.png', '1', '1', '37', 'Gusau', 'Noth-West', 'Ahmad Sani Yerima', 'PDP', 1999, 2003, 5, '4', '2', 'yes', '2015-06-17 10:30:56', '2015-06-17 10:30:56'),
(398, 'peter-obi', 'blank.png', '1', '1', '1', 'Akwa', 'South-East', 'Peter Obi', 'APGA', 2006, 2010, 3, '4', '2', 'yes', '2015-06-17 10:56:55', '2015-06-17 10:56:55'),
(399, 'virginia-etiaba', 'blank.png', '1', '1', '1', 'Akwa', 'South-East', 'Virginia Etiaba', 'APGA', 2006, 2010, 11, '1', '1', 'no', '2015-06-17 10:57:47', '2015-06-17 10:57:47'),
(400, 'sullivan-chime', 'blank.png', '1', '1', '2', 'Enugu', 'South-East', 'Sullivan Chime', 'PDP', 2007, 0, 3, '4', '1', 'yes', '2015-06-17 10:58:36', '2015-06-17 10:58:36'),
(401, 'victor-attah', 'blank.png', '1', '1', '3', 'Uyo', 'South-South', 'Victor Attah', 'PDP', 1999, 2007, 5, '4', '2', 'yes', '2015-06-17 10:59:17', '2015-06-17 10:59:17'),
(402, 'murtala-nyako', 'blank.png', '1', '1', '4', 'Yola', 'North-East', 'Murtala Nyako', 'PDP', 2007, 0, 5, '1', '0', NULL, '2015-06-17 10:59:53', '2015-06-17 10:59:53'),
(403, 'theodore-a.-orji', 'blank.png', '1', '1', '5', 'Umuahia', 'South-East', 'Theodore A. Orji', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:00:35', '2015-06-17 11:00:35'),
(404, 'isa-yuguda', 'blank.png', '1', '1', '6', 'Bauchi', 'North-East', 'Isa Yuguda', 'ANPP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:01:13', '2015-06-17 11:01:13'),
(405, 'goodluck-jonathan', 'blank.png', '1', '1', '7', 'Yenagoa', 'South-South', 'Goodluck Jonathan', 'PDP', 2005, 0, 12, '2', '1', 'no', '2015-06-17 11:02:25', '2015-06-17 11:02:25'),
(406, 'timipre-sylva', 'blank.png', '1', '1', '7', 'Yenagoa', 'South-South', 'Timipre Sylva', 'PDP', 2007, 0, 5, '1', '1', 'yes', '2015-06-17 11:08:52', '2015-06-17 11:08:52'),
(407, 'werinipre-seibarugo', 'blank.png', '1', '1', '7', 'Yenagoa', 'South-South', 'Werinipre Seibarugo', 'PDP', 2008, 0, 4, '1', '1', 'no', '2015-06-17 11:09:38', '2015-06-17 11:09:38'),
(408, 'timipre-sylva', 'blank.png', '1', '1', '7', 'Yenagoa', 'South-South', 'Timipre Sylva', 'PDP', 2008, 0, 5, '1', '2', 'yes', '2015-06-17 11:10:24', '2015-06-17 11:10:24'),
(409, 'gabriel-suswam', 'blank.png', '1', '1', '8', 'Makurdi', 'Central', 'Gabriel Suswam', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:11:03', '2015-06-17 11:11:03'),
(410, 'ali-modu-sheriff', 'blank.png', '1', '1', '10', 'Maiduguri', 'North-East', 'Ali Modu Sheriff', 'PDP', 2003, 2007, 5, '4', '2', 'yes', '2015-06-17 11:11:54', '2015-06-17 11:11:54'),
(411, 'liyel-imoke', 'blank.png', '1', '1', '11', 'Calabar', 'South-South', 'Liyel Imoke', 'PDP', 2007, 0, 5, '2', '1', 'yes', '2015-06-17 11:12:34', '2015-06-17 11:12:34'),
(412, 'emmanuel-uduaghan', 'blank.png', '1', '1', '12', 'Asaba', 'South-South', 'Emmanuel Uduaghan', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:23:58', '2015-06-17 11:23:58'),
(413, 'martin-elechi', 'blank.png', '1', '1', '13', 'Abakaliki', 'South-East', 'Martin Elechi', 'PDP', 2007, 0, 5, '2', '1', 'yes', '2015-06-17 11:30:42', '2015-06-17 11:30:42'),
(414, 'professor-oserheimen-osunbor', 'blank.png', '1', '1', '14', 'Benin-City', 'South-South', 'Professor Oserheimen Osunbor', 'PDP', 2007, 0, 5, '2', '1', 'yes', '2015-06-17 11:32:16', '2015-06-17 11:32:16'),
(415, 'comrade-adams-a.-oshiomhole', 'blank.png', '1', '1', '14', 'Benin-City', 'South-South', 'Comrade Adams A. Oshiomhole', 'AC', 2008, 0, 11, '4', '1', 'yes', '2015-06-17 11:36:11', '2015-06-17 11:36:11'),
(416, 'dr.-kayode-fayemi', 'blank.png', '1', '1', '15', 'Ado Ekiti', 'South-West', 'Dr. Kayode Fayemi', 'PDP', 2010, 0, 10, '4', '1', 'yes', '2015-06-17 11:36:48', '2015-06-17 11:36:48'),
(417, 'mohammed-danjuma-goje', 'blank.png', '1', '1', '17', 'Gombe', 'North-East', 'Mohammed Danjuma Goje', 'PDP', 2003, 0, 5, '4', '1', 'yes', '2015-06-17 11:37:35', '2015-06-17 11:37:35'),
(418, 'ikedi-g.-ohakim', 'blank.png', '1', '1', '18', 'Owerri', 'South-South', 'Ikedi G. Ohakim', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:40:38', '2015-06-17 11:40:38'),
(419, 'sule-lamido', 'blank.png', '1', '1', '19', 'Dutse', 'North-Central', 'Sule Lamido', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:41:14', '2015-06-17 11:41:14'),
(420, 'mohammed-namadi-sambo', 'blank.png', '1', '1', '20', 'Kaduna', 'North-West', 'Mohammed Namadi Sambo', 'PDP', 2007, 0, 5, '3', '1', 'yes', '2015-06-17 11:41:47', '2015-06-17 11:41:47'),
(421, 'patrick-ibrahim-yakowa', 'blank.png', '1', '1', '20', 'Kaduna', 'North-West', 'Patrick Ibrahim Yakowa', 'PDP', 2010, 0, 3, '4', '1', NULL, '2015-06-17 11:42:34', '2015-06-17 11:42:34'),
(422, 'malam-ibrahim-shekarau', 'blank.png', '1', '1', '21', 'Kano', 'North-West', 'Malam Ibrahim Shekarau', 'PDP', 2003, 2007, 5, '4', '2', 'yes', '2015-06-17 11:43:17', '2015-06-17 11:43:17'),
(423, 'ibrahim-shema', 'blank.png', '1', '1', '22', 'Katsina', 'North', 'Ibrahim Shema', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:43:49', '2015-06-17 11:43:49'),
(424, 'usman-saidu-nasamu-dakingari', 'blank.png', '1', '1', '23', 'Birnin Kebbi', 'North-West', 'Usman Saidu Nasamu Dakingari', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:44:31', '2015-06-17 11:44:31'),
(425, 'clarence-olafemi', 'blank.png', '1', '1', '24', 'Lokoja', 'Central', 'Clarence Olafemi', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:45:01', '2015-06-17 11:45:01'),
(426, 'bukola-saraki', 'blank.png', '1', '1', '25', 'Illorin', 'West', 'Bukola Saraki', 'PDP', 2003, 2007, 5, '4', '2', 'yes', '2015-06-17 11:46:10', '2015-06-17 11:46:10'),
(427, 'mr-babatunde-fashola', 'blank.png', '1', '1', '26', 'Ikeja', 'South-West', 'Mr Babatunde Fashola', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:46:43', '2015-06-17 11:46:43'),
(428, 'aliyu-doma', 'blank.png', '1', '1', '27', 'Lafia', 'Central', 'Aliyu Doma', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:47:23', '2015-06-17 11:47:23'),
(429, 'mu''azu-babangida-aliyu', 'blank.png', '1', '1', '28', 'Minna', 'North-West', 'Mu''azu Babangida Aliyu', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:48:05', '2015-06-17 11:48:05'),
(430, 'gbenga-daniel', 'blank.png', '1', '1', '29', 'Abeokuta', 'South-West', 'Gbenga Daniel', 'PDP', 2003, 2007, 5, '4', '2', 'yes', '2015-06-17 11:48:35', '2015-06-17 11:48:35'),
(431, 'olusegun-mimiko', 'blank.png', '1', '1', '30', 'Akure', 'South-West', 'Olusegun Mimiko', 'PDP', 2009, 0, 5, '4', '1', 'yes', '2015-06-17 11:49:02', '2015-06-17 11:49:02'),
(432, 'olagunsoye-oyinlola', 'blank.png', '1', '1', '31', 'Osogbo', 'South-West', 'Olagunsoye Oyinlola', 'PDP', 2007, 2011, 5, '4', '2', 'yes', '2015-06-17 11:49:44', '2015-06-17 11:49:44'),
(433, 'christopher-alao-akala', 'blank.png', '1', '1', '32', 'Ibadan', 'South-West', 'Christopher Alao-Akala', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:50:16', '2015-06-17 11:50:16'),
(434, 'celestine-omehia', 'blank.png', '1', '1', '34', 'Port Harcourt', 'South-South', 'Celestine Omehia', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:50:45', '2015-06-17 11:50:45'),
(435, 'danbaba-danfulani-suntai', 'blank.png', '1', '1', '36', 'Jalingo', 'East', 'Danbaba Danfulani Suntai', 'PDP', 2007, 0, 5, '4', '1', NULL, '2015-06-17 11:51:26', '2015-06-17 11:51:26'),
(436, 'mamman-bello-ali', 'blank.png', '1', '1', '38', 'Damaturu', 'North-East', 'Mamman Bello Ali', 'PDP', 2007, 0, 5, '2', '1', 'yes', '2015-06-17 11:51:51', '2015-06-17 11:51:51'),
(437, 'mahmud-shinkafi', 'blank.png', '1', '1', '37', 'Gusau', 'Noth-West', 'Mahmud Shinkafi', 'PDP', 2007, 0, 5, '4', '1', 'yes', '2015-06-17 11:52:24', '2015-06-17 11:52:24'),
(438, 'peter-obi', 'blank.png', '1', '1', '1', 'Akwa', 'South-East', 'Peter Obi', 'APGA', 2010, 0, 3, '2', '2', 'yes', '2015-06-17 13:23:58', '2015-06-17 13:23:58'),
(439, 'sullivan-chime', 'blank.png', '1', '1', '2', 'Enugu', 'South-East', 'Sullivan Chime', 'PDP', 2007, 2011, 4, '5', '2', 'yes', '2015-06-17 13:26:50', '2015-06-17 13:26:50'),
(440, 'godswill-akpabio', 'blank.png', '1', '1', '3', 'Uyo', 'South-South', 'Godswill Akpabio', 'PDP', 2007, 2011, 4, '5', '2', NULL, '2015-06-17 13:28:14', '2015-06-17 13:28:14'),
(441, 'james-bala-ngilari', 'blank.png', '1', '1', '4', 'Yola', 'North-East', 'James Bala Ngilari', 'PDP', 2014, 0, 10, '1', '1', 'no', '2015-06-17 13:30:59', '2015-06-17 13:30:59'),
(442, 'theodore-orji', 'blank.png', '1', '1', '5', 'Umuahia', 'South-East', 'Theodore Orji', 'PDP', 2007, 2011, 5, '5', '2', NULL, '2015-06-17 13:32:07', '2015-06-17 13:32:07'),
(443, 'isa-yuguda', 'blank.png', '1', '1', '6', 'Bauchi', 'North-East', 'Isa Yuguda', 'PDP', 2007, 2011, 4, '5', '2', NULL, '2015-06-17 13:32:51', '2015-06-17 13:32:51'),
(444, 'nestor-binabo', 'blank.png', '1', '1', '7', 'Yenagoa', 'South-South', 'Nestor Binabo', 'PDP', 2012, 0, 1, '1', '1', 'no', '2015-06-17 13:33:48', '2015-06-17 13:33:48'),
(445, 'henry-seriake-dickson', 'blank.png', '1', '1', '7', 'Yenagoa', 'South-South', 'Henry Seriake Dickson', 'PDP', 2012, 0, 2, '3', '1', 'yes', '2015-06-17 13:34:44', '2015-06-17 13:34:44'),
(446, 'gabriel-torwua-suswam', 'blank.png', '1', '1', '8', 'Makurdi', 'Central', 'Gabriel Torwua Suswam', 'PDP', 2007, 2011, 5, '5', '2', NULL, '2015-06-17 13:35:35', '2015-06-17 13:35:35'),
(447, 'kashim-shettima', 'blank.png', '1', '1', '10', 'Maiduguri', 'North-East', 'Kashim Shettima', 'APC', 2011, 2010, 4, '4', '1', NULL, '2015-06-17 13:36:05', '2015-06-17 13:36:05'),
(448, 'liyel-imoke', 'blank.png', '1', '1', '11', 'Calabar', 'South-South', 'Liyel Imoke', 'PDP', 2007, 2011, 4, '5', '2', NULL, '2015-06-17 13:36:45', '2015-06-17 13:36:45'),
(449, 'emmanuel-uduaghan', 'blank.png', '1', '1', '12', 'Asaba', 'South-South', 'Emmanuel Uduaghan', 'PDP', 2007, 2011, 5, '5', '2', NULL, '2015-06-17 13:37:14', '2015-06-17 13:37:14'),
(450, 'martin-elechi', 'blank.png', '1', '1', '13', 'Abakaliki', 'South-East', 'Martin Elechi', 'PDP', 2007, 2011, 3, '5', '2', NULL, '2015-06-17 13:37:45', '2015-06-17 13:37:45'),
(451, 'adams-oshiomhole', 'blank.png', '1', '1', '14', 'Benin-City', 'South-South', 'Adams Oshiomhole', 'APC', 2008, 2012, 4, '5', '2', NULL, '2015-06-17 13:38:16', '2015-06-17 13:38:16'),
(452, 'ayo-fayose', 'blank.png', '1', '1', '15', 'Ado Ekiti', 'South-West', 'Ayo Fayose', 'PDP', 2014, 0, 10, '1', '1', NULL, '2015-06-17 13:38:58', '2015-06-17 13:38:58'),
(453, 'ibrahim-dankwambo', 'blank.png', '1', '1', '17', 'Gombe', 'North-East', 'Ibrahim Dankwambo', 'PDP', 2011, 2010, 4, '4', '1', NULL, '2015-06-17 13:39:27', '2015-06-17 13:39:27'),
(454, 'rochas-anayo-okorocha', 'blank.png', '1', '1', '18', 'Owerri', 'South-South', 'Rochas Anayo Okorocha', 'APC', 2011, 0, 3, '4', '1', NULL, '2015-06-17 13:40:11', '2015-06-17 13:40:11'),
(455, 'sule-lamido', 'blank.png', '1', '1', '19', 'Dutse', 'North-Central', 'Sule Lamido', 'PDP', 2007, 2011, 4, '5', '2', NULL, '2015-06-17 13:40:44', '2015-06-17 13:40:44'),
(456, 'mukhtar-yero', 'blank.png', '1', '1', '20', 'Kaduna', 'North-West', 'Mukhtar Yero', 'PDP', 2012, 0, 12, '3', '1', 'no', '2015-06-17 13:41:23', '2015-06-17 13:41:23'),
(457, 'dr-rabi''u-musa-kwankwaso', 'blank.png', '1', '1', '21', 'Kano', 'North-West', 'Dr Rabi''u Musa Kwankwaso', 'APC', 2011, 0, 5, '4', '1', NULL, '2015-06-17 13:42:06', '2015-06-17 13:42:06'),
(458, 'ibrahim-shema', 'blank.png', '1', '1', '22', 'Katsina', 'North', 'Ibrahim Shema', 'PDP', 2007, 2011, 5, '5', '2', NULL, '2015-06-17 14:08:46', '2015-06-17 14:08:46'),
(459, 'saidu-usman-nasamu-dakingari', 'blank.png', '1', '1', '23', 'Birnin Kebbi', 'North-West', 'Saidu Usman Nasamu Dakingari', 'PDP', 2007, 2011, 4, '5', '2', NULL, '2015-06-17 14:09:34', '2015-06-17 14:09:34'),
(460, 'ibrahim-idris', 'blank.png', '1', '1', '24', 'Lokoja', 'Central', 'Ibrahim Idris', 'PDP', 2012, 0, 1, '3', '1', NULL, '2015-06-17 14:10:09', '2015-06-17 14:10:09'),
(461, 'abdulfatah-ahmed', 'blank.png', '1', '1', '25', 'Illorin', 'West', 'Abdulfatah Ahmed', 'APC', 2011, 2014, 4, '4', '1', NULL, '2015-06-17 14:11:11', '2015-06-17 14:11:11'),
(462, 'babatunde-fashola', 'blank.png', '1', '1', '26', 'Ikeja', 'South-West', 'Babatunde Fashola', 'APC', 2007, 2011, 4, '5', '2', NULL, '2015-06-17 14:13:04', '2015-06-17 14:13:04'),
(463, 'umaru-tanko-al-makura', 'blank.png', '1', '1', '27', 'Lafia', 'Central', 'Umaru Tanko Al-Makura', 'APC', 2011, 2014, 4, '4', '1', NULL, '2015-06-17 14:14:17', '2015-06-17 14:14:17'),
(464, 'mu''azu-babangida-aliyu', 'blank.png', '1', '1', '28', 'Minna', 'North-West', 'Mu''azu Babangida Aliyu', 'PDP', 2007, 2011, 4, '5', '1', NULL, '2015-06-17 14:15:29', '2015-06-17 14:15:29'),
(465, 'ibikunle-amosun', 'blank.png', '1', '1', '29', 'Abeokuta', 'South-West', 'Ibikunle Amosun', 'APC', 2011, 2014, 5, '4', '1', NULL, '2015-06-17 14:16:03', '2015-06-17 14:16:03'),
(466, 'olusegun-mimiko', 'blank.png', '1', '1', '30', 'Akure', 'South-West', 'Olusegun Mimiko', 'LP', 2009, 2013, 2, '5', '2', NULL, '2015-06-17 14:18:41', '2015-06-17 14:18:41'),
(467, 'rauf-aregbesola', 'blank.png', '1', '1', '31', 'Osogbo', 'South-West', 'Rauf Aregbesola', 'APC', 2010, 2014, 11, '5', '2', NULL, '2015-06-17 14:29:49', '2015-06-17 14:29:49'),
(468, 'abiola-ajimobi', 'blank.png', '1', '1', '32', 'Ibadan', 'South-West', 'Abiola Ajimobi', 'CAN', 2011, 2014, 5, '4', '1', NULL, '2015-06-17 14:30:29', '2015-06-17 14:30:29'),
(469, 'jonah-david-jang', 'blank.png', '1', '1', '33', 'Jos', 'Central', 'Jonah David Jang', 'PDP', 2007, 2011, 4, '5', '2', NULL, '2015-06-17 14:31:00', '2015-06-17 14:31:00'),
(470, 'chibuike-amaechi', 'blank.png', '1', '1', '34', 'Port Harcourt', 'South-South', 'Chibuike Amaechi', 'APC', 2007, 2011, 10, '5', '2', NULL, '2015-06-17 14:31:33', '2015-06-17 14:31:33'),
(471, 'aliyu-wamakko', 'blank.png', '1', '1', '35', 'Sokoto', 'North-West', 'Aliyu Wamakko', 'PDP', 2007, 2011, 5, '5', '2', NULL, '2015-06-17 14:32:05', '2015-06-17 14:32:05'),
(472, 'danbaba-danfulani-suntai', 'blank.png', '1', '1', '36', 'Jalingo', 'East', 'Danbaba Danfulani Suntai', 'PDP', 2007, 2011, 4, '5', '2', NULL, '2015-06-17 14:32:44', '2015-06-17 14:32:44'),
(473, 'ibrahim-geidam', 'blank.png', '1', '1', '38', 'Damaturu', 'North-East', 'Ibrahim Geidam', 'ANPP', 2009, 2011, 1, '5', '2', 'no', '2015-06-17 14:33:29', '2015-06-17 14:33:29'),
(474, 'abdul''aziz-abubakar-yari', 'blank.png', '1', '1', '37', 'Gusau', 'North-West', 'Abdul''aziz Abubakar Yari', 'ANPP', 2011, 2014, 5, '4', '1', 'yes', '2015-06-17 14:34:11', '2015-06-17 14:34:11');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `initial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` double(8,2) DEFAULT NULL,
  `country_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capital_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `government` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `population` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `temperature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `initial`, `timezone`, `country_name`, `flag`, `map`, `capital`, `capital_id`, `language`, `government`, `population`, `area`, `temperature`, `active`, `created_at`, `updated_at`) VALUES
(1, 'NG', 1.00, 'Nigeria', 'ng.png', 'ng.png', 'abuja', '', '', '', '', '', NULL, 'on', '2015-06-15 22:05:25', '2015-06-15 22:05:25'),
(2, 'AF', NULL, 'Afghanistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-06-14 15:46:18', '2015-06-14 15:46:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_06_11_170218_create_appointment_table', 1),
('2015_06_13_002835_create_country_table', 1),
('2015_06_13_010609_create_position_table', 1),
('2015_06_13_104114_create_pages_table', 1),
('2015_06_13_224505_create_settings_table', 2),
('2015_06_14_105439_update_country_table', 3),
('2015_06_15_084516_update_positions_table', 4),
('2015_06_15_093037_create_state_table', 4),
('2015_06_15_111523_add_country_timezone', 4),
('2015_06_15_145313_update_country_table', 4),
('2015_06_16_082349_add_appointments_avatar', 5);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `slug`, `page_title`, `page_content`, `created_at`, `updated_at`) VALUES
(1, 'about-us', 'About Us', '<p>Lorem Ipsum s</p>', '0000-00-00 00:00:00', '2015-06-13 13:50:15'),
(2, 'contribute', 'Contribute', 'Contribute Page', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'contact-us', 'Contact Us', 'Lorem Ipsum', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE IF NOT EXISTS `positions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `slug`, `position_name`, `created_at`, `updated_at`) VALUES
(1, 'governor', 'Governor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'house', 'House of Representative', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'senator', 'Senator', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'president', 'President', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `name_setting` text COLLATE utf8_unicode_ci NOT NULL,
  `name_value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `slug`, `name_setting`, `name_value`, `created_at`, `updated_at`) VALUES
(1, 'site-name', 'Site Name', 'WorldPols', '2015-06-14 06:33:00', '2015-06-14 06:33:47'),
(2, 'facebook', 'Facebook', 'http://www.facebook.com', '2015-06-14 06:33:00', '2015-06-14 06:33:00'),
(3, 'twitter', 'Twitter', 'http://www.twitter.com', '2015-06-14 06:33:00', '2015-06-14 06:33:00');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_name` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `slug`, `country_id`, `state_name`, `created_at`, `updated_at`) VALUES
(1, 'anambra', 1, 'Anambra', '2015-06-15 21:53:52', '2015-06-15 21:53:52'),
(2, 'enugu', 1, 'Enugu', '2015-06-15 21:53:58', '2015-06-15 21:53:58'),
(3, 'akwa-ibom', 1, 'Akwa Ibom', '2015-06-15 21:54:05', '2015-06-15 21:54:05'),
(4, 'adamawa', 1, 'Adamawa', '2015-06-15 21:54:11', '2015-06-15 21:54:11'),
(5, 'abia', 1, 'Abia', '2015-06-15 21:55:36', '2015-06-15 21:55:36'),
(6, 'bauchi', 1, 'Bauchi', '2015-06-15 21:55:43', '2015-06-15 21:55:43'),
(7, 'bayelsa', 1, 'Bayelsa', '2015-06-15 21:55:49', '2015-06-15 21:55:49'),
(8, 'benue', 1, 'Benue', '2015-06-15 21:55:56', '2015-06-15 21:55:56'),
(9, 'abuja', 1, 'Abuja', '2015-06-16 09:05:45', '2015-06-16 09:05:45'),
(10, 'borno', 1, 'Borno', '2015-06-16 09:07:10', '2015-06-16 09:07:10'),
(11, 'cross-river', 1, 'Cross River', '2015-06-16 09:07:54', '2015-06-16 09:07:54'),
(12, 'delta', 1, 'Delta', '2015-06-16 09:08:31', '2015-06-16 09:08:31'),
(13, 'ebonyi', 1, 'Ebonyi', '2015-06-16 09:09:04', '2015-06-16 09:09:04'),
(14, 'edo', 1, 'Edo', '2015-06-16 09:09:29', '2015-06-16 09:09:29'),
(15, 'ekiti', 1, 'Ekiti', '2015-06-16 09:09:52', '2015-06-16 09:09:52'),
(16, 'fct', 1, 'FCT', '2015-06-16 09:11:00', '2015-06-16 09:11:00'),
(17, 'gombe', 1, 'Gombe', '2015-06-16 09:11:32', '2015-06-16 09:11:32'),
(18, 'imo', 1, 'Imo', '2015-06-16 09:11:53', '2015-06-16 09:11:53'),
(19, 'jigawa', 1, 'Jigawa', '2015-06-16 09:12:21', '2015-06-16 09:12:21'),
(20, 'kaduna', 1, 'Kaduna', '2015-06-16 09:12:50', '2015-06-16 09:12:50'),
(21, 'kano', 1, 'Kano', '2015-06-16 09:13:13', '2015-06-16 09:13:13'),
(22, 'katsina', 1, 'Katsina', '2015-06-16 09:13:45', '2015-06-16 09:13:45'),
(23, 'kebbi', 1, 'Kebbi', '2015-06-16 09:14:19', '2015-06-16 09:14:19'),
(24, 'kogi', 1, 'Kogi', '2015-06-16 09:14:40', '2015-06-16 09:14:40'),
(25, 'kwara', 1, 'Kwara', '2015-06-16 09:15:25', '2015-06-16 09:15:25'),
(26, 'lagos', 1, 'Lagos', '2015-06-16 09:15:43', '2015-06-16 09:15:43'),
(27, 'nasarawa', 1, 'Nasarawa', '2015-06-16 09:16:31', '2015-06-16 09:16:31'),
(28, 'niger', 1, 'Niger', '2015-06-16 09:16:52', '2015-06-16 09:16:52'),
(29, 'ogun', 1, 'Ogun', '2015-06-16 09:17:09', '2015-06-16 09:17:09'),
(30, 'ondo', 1, 'Ondo', '2015-06-16 09:17:31', '2015-06-16 09:17:31'),
(31, 'osun', 1, 'Osun', '2015-06-16 09:17:58', '2015-06-16 09:17:58'),
(32, 'oyo', 1, 'Oyo', '2015-06-16 09:18:19', '2015-06-16 09:18:19'),
(33, 'plateau', 1, 'Plateau', '2015-06-16 09:18:47', '2015-06-16 09:18:47'),
(34, 'rivers', 1, 'Rivers', '2015-06-16 09:19:16', '2015-06-16 09:19:16'),
(35, 'sokoto', 1, 'Sokoto', '2015-06-16 09:19:37', '2015-06-16 09:19:37'),
(36, 'taraba', 1, 'Taraba', '2015-06-16 09:20:17', '2015-06-16 09:20:17'),
(37, 'zamfara', 1, 'Zamfara', '2015-06-16 09:24:08', '2015-06-16 09:24:08'),
(38, 'yobe', 1, 'Yobe', '2015-06-16 09:24:53', '2015-06-16 09:24:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `avatar`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'codegap', 'blank', 'info@codegap.co.uk', '$2y$10$wSFt3BXuCaLJFoe/K5XEdOSIpWFiUBMk2nAwrSt839pWUuAQfEhrK', 'g6nHgCfwILR9PTV6ZXAimx7pdGxeBewnyI9d7cnVFhSZaowVHIZxFeguIYs4', '2015-06-13 13:34:40', '2015-06-17 21:13:09'),
(2, 'Site Admin', 'Admin', 'siteadmin', NULL, 'test@codegap.co.uk', '$2y$10$wSFt3BXuCaLJFoe/K5XEdOSIpWFiUBMk2nAwrSt839pWUuAQfEhrK', '2D55KvcSuBpvCaY6RrSoFmvMWEF2dNlwpWn31bu6h5ru99qMTLEoewq3z3zu', '2015-06-13 13:37:45', '2015-06-13 17:50:13');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
