# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.25)
# Database: worldpols
# Generation Time: 2015-06-27 12:43:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table appointments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `appointments`;

CREATE TABLE `appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'blank.png',
  `position_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `party` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term_a` int(10) unsigned NOT NULL,
  `term_b` int(10) unsigned NOT NULL,
  `month` int(10) unsigned NOT NULL,
  `office_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elect` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;

INSERT INTO `appointments` (`id`, `slug`, `avatar`, `position_id`, `country_id`, `state`, `capital`, `region`, `full_name`, `party`, `term_a`, `term_b`, `month`, `office_time`, `term`, `elect`, `created_at`, `updated_at`)
VALUES
	(2,'chinwoke-mbadinuju','blank.png','2','1','1','','','Chinwoke Mbadinuju','',1999,1990,5,'4','1','yes','2015-06-13 01:37:46','2015-06-16 07:45:02'),
	(3,'chimaroke-nnamani','blank.png','1','1','4','Enugu','South-East','Chimaroke Nnamani','',1999,1990,5,'4','1','yes','2015-06-13 13:37:05','2015-06-16 07:44:46'),
	(4,'victor-attah','blank.png','2','1','5','Uyo','South-South','Victor Attah','',1999,1990,5,'4','2','no','2015-06-13 13:39:14','2015-06-16 07:45:10'),
	(5,'boni-harun','blank.png','1','1','6','Yola','North-East','Boni Harun','',1999,1990,5,'4','1','yes','2015-06-13 13:41:46','2015-06-16 07:44:23'),
	(6,'test','test','1','3','0','','','test','',0,0,1,'1','0',NULL,'2015-06-16 09:23:20','2015-06-16 09:23:20'),
	(7,'asd-fdf','asd-fdf.jpg','1','1','1','','','asd fdf','',1999,1990,1,'1','1',NULL,'2015-06-16 09:28:38','2015-06-17 08:35:02'),
	(8,'asdf-sdf','asdf sdf','0','0','0','','','asdf sdf','',0,0,1,'1','0',NULL,'2015-06-16 09:30:22','2015-06-16 09:30:22'),
	(9,'dd-fff','dd fff','0','0','0','','','dd fff','',0,0,1,'1','0',NULL,'2015-06-16 09:31:05','2015-06-16 09:31:05'),
	(10,'ff-gh-xxx','ff-gh-xxx.jpg','0','0','0','','','ff gh xxx','',0,0,1,'1','0',NULL,'2015-06-16 09:31:50','2015-06-16 09:31:50'),
	(11,'test','blank.png','0','7','0','','','test','',0,0,1,'1','0',NULL,'2015-06-17 08:28:26','2015-06-17 08:28:26'),
	(12,'test','blank.png','1','1','0','','','test','',0,0,1,'1','0',NULL,'2015-06-17 08:28:40','2015-06-17 08:28:40'),
	(13,'adsf-sd-fsdf','blank.png','1','0','0','','','adsf sd fsdf','',1990,1990,1,'1','1',NULL,'2015-06-17 08:28:50','2015-06-17 09:04:22'),
	(14,'aswe-dfdf','blank.png','4','0','0','','','aswe dfdf','',1990,1990,1,'1','1',NULL,'2015-06-17 08:28:56','2015-06-17 08:48:30'),
	(15,'df-xd-c','blank.png','1','0','0','','','df xd c','',1990,1990,1,'1','1',NULL,'2015-06-17 08:29:01','2015-06-17 10:04:02'),
	(16,'aeerer','blank.png','1','0','0','','','aeerer','',0,0,1,'1','0',NULL,'2015-06-17 08:30:29','2015-06-17 08:30:29'),
	(17,'aaaaaaaa','blank.png','2','0','0','','','aaaaaaaa','',0,0,1,'1','0',NULL,'2015-06-17 08:30:46','2015-06-17 08:30:46'),
	(18,'a55555','blank.png','1','0','0','','','a55555','',0,0,1,'1','0',NULL,'2015-06-17 08:31:14','2015-06-17 08:31:14'),
	(19,'attttttttt','blank.png','1','0','0','','','attttttttt','',1990,1990,1,'1','1',NULL,'2015-06-17 08:32:09','2015-06-17 08:35:07'),
	(20,'aatttrtr','blank.png','4','0','0','','','aatttrtr','',0,0,1,'1','0',NULL,'2015-06-17 08:50:16','2015-06-17 08:50:16'),
	(21,'aaa','blank.png','2','0','0','','','aaa','',1990,1990,1,'1','1',NULL,'2015-06-17 08:51:42','2015-06-17 09:04:34'),
	(22,'af-df-sdf-','blank.png','1','0','0','','','af df sdf ','',0,0,1,'1','0',NULL,'2015-06-17 08:51:56','2015-06-17 08:51:56'),
	(23,'aaaa','blank.png','1','0','0','','','aaaa','',0,0,1,'1','0',NULL,'2015-06-17 09:00:32','2015-06-17 09:00:32'),
	(24,'aadfdfdf','blank.png','0','0','0','','','aadfdfdf','',0,0,1,'1','0',NULL,'2015-06-17 09:00:37','2015-06-17 09:00:37'),
	(25,'asdfdfdf','blank.png','0','0','0','','','asdfdfdf','',0,0,1,'1','0',NULL,'2015-06-17 09:00:43','2015-06-17 09:00:43'),
	(26,'aasdf-df','blank.png','3','0','0','','','aasdf df','',1990,1990,1,'1','1',NULL,'2015-06-17 09:00:50','2015-06-17 09:05:24');

/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `initial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` float DEFAULT NULL,
  `country_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capital_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `government` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `population` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `temperature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` (`id`, `initial`, `timezone`, `country_name`, `flag`, `map`, `capital`, `capital_id`, `language`, `government`, `population`, `area`, `temperature`, `active`, `created_at`, `updated_at`)
VALUES
	(1,'NG',1,'Nigeria',NULL,NULL,'Abuja','254085',NULL,NULL,NULL,NULL,'40deg','on','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,'AF',4.3,'Afghanistan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-06-14 11:10:00','2015-06-14 13:11:48'),
	(3,'AL',1,'Albania',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'on','2015-06-14 11:13:08','2015-06-14 11:13:08'),
	(4,'DZ',1,'Algeria',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'on','2015-06-14 11:15:44','2015-06-14 11:15:44'),
	(5,'AS',-11,'American Samoa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'on','2015-06-14 11:17:33','2015-06-14 11:17:33'),
	(6,'AD',1,'Andorra',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'on','2015-06-14 11:18:14','2015-06-14 11:18:14'),
	(7,'AO',1,'Angola',NULL,NULL,'','','','','','',NULL,'on','2015-06-15 22:38:26','2015-06-15 22:38:26'),
	(8,'AS',-4,'Anguilla',NULL,NULL,'','','','','','',NULL,'on','2015-06-17 22:24:06','2015-06-17 22:24:06'),
	(10,'AD',-8,'Philippines',NULL,NULL,'','','','','','',NULL,NULL,'2015-06-17 22:24:11','2015-06-17 22:24:11'),
	(11,'AF',8,'Philippines',NULL,NULL,'manila','254085','Tagalog','Parliament','23,3000','23sq',NULL,NULL,'2015-06-17 22:24:16','2015-06-17 22:24:16'),
	(12,'AE',-12,'Philippines',NULL,NULL,'manila','264885','','','','',NULL,NULL,'2015-06-17 22:24:33','2015-06-17 22:24:33'),
	(13,'PH',-12,'Philippines',NULL,NULL,'manila','264885','','','','',NULL,NULL,'2015-06-15 16:00:49','2015-06-15 16:00:49'),
	(14,'PH',-12,'Philippines',NULL,NULL,'manila','264885','','','','',NULL,NULL,'2015-06-15 16:01:26','2015-06-15 16:01:26'),
	(15,'PH',-12,'Philippines',NULL,NULL,'manila','264885','','','','',NULL,NULL,'2015-06-15 16:01:41','2015-06-15 16:01:41'),
	(16,'PH',-12,'Philippines',NULL,NULL,'manila','264885','','','','',NULL,NULL,'2015-06-17 22:04:36','2015-06-17 22:04:36'),
	(17,'PHW',-12,'Philippines',NULL,NULL,'manila','264885','','','','',NULL,NULL,'2015-06-15 16:03:27','2015-06-15 16:03:27'),
	(18,'PH',-12,'Philippines',NULL,NULL,'manila','264885','','','','',NULL,NULL,'2015-06-15 16:03:48','2015-06-15 16:03:48'),
	(19,'AG',-12,'Rome',NULL,NULL,'roma','2r3234','','','','',NULL,NULL,'2015-06-17 22:24:01','2015-06-17 22:24:01'),
	(20,'RO',-12,'Rome',NULL,NULL,'roma','2r3234','','','','',NULL,NULL,'2015-06-15 16:07:31','2015-06-15 16:07:31'),
	(21,'RO',-12,'Rome',NULL,NULL,'roma','2r3234','','','','',NULL,NULL,'2015-06-15 16:08:03','2015-06-15 16:08:03'),
	(22,'RO',-12,'Rome',NULL,NULL,'roma','2r3234','','','','',NULL,NULL,'2015-06-15 16:08:23','2015-06-15 16:08:23'),
	(23,'US',3,'New York','ny.png','ny.png','new york','349727','english','gov','2323','2454545',NULL,'on','2015-06-17 22:04:14','2015-06-17 22:04:14'),
	(24,'PH',-12,'Asdf',NULL,NULL,'',NULL,'','','','',NULL,NULL,'2015-06-17 22:03:43','2015-06-17 22:03:43');

/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2014_10_12_000000_create_users_table',1),
	('2014_10_12_100000_create_password_resets_table',1),
	('2015_06_11_170218_create_appointment_table',1),
	('2015_06_13_002835_create_country_table',1),
	('2015_06_13_010609_create_position_table',1),
	('2015_06_13_104114_create_pages_table',2),
	('2015_06_13_224505_create_settings_table',3),
	('2015_06_14_105439_update_country_table',4),
	('2015_06_15_084516_update_positions_table',5),
	('2015_06_15_093037_create_state_table',6),
	('2015_06_15_111523_add_country_timezone',7),
	('2015_06_15_145313_update_country_table',8),
	('2015_06_16_082349_add_appointments_avatar',9);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `slug`, `page_title`, `page_content`, `created_at`, `updated_at`)
VALUES
	(1,'about-us','About Us','<p><strong>Lorem Ipsum</strong></p>','0000-00-00 00:00:00','2015-06-13 11:53:44'),
	(2,'contribute','Contribute','<p><em>Contribute Pages</em></p>','0000-00-00 00:00:00','2015-06-13 13:43:24'),
	(3,'contact-us','Contact Us','Lorem Ipsum','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table positions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `positions`;

CREATE TABLE `positions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `positions` WRITE;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;

INSERT INTO `positions` (`id`, `slug`, `position_name`, `created_at`, `updated_at`)
VALUES
	(1,'governor','Governor','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,'house','House of Representative','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(3,'senator','Senator','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(4,'president','President','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `positions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `name_setting` text COLLATE utf8_unicode_ci NOT NULL,
  `name_value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `slug`, `name_setting`, `name_value`, `created_at`, `updated_at`)
VALUES
	(1,'site-name','Site Name','WorldPolss','2015-06-13 23:06:18','2015-06-14 07:26:59'),
	(2,'facebook','Facebook','http://www.facebook.com','2015-06-13 23:06:18','2015-06-14 06:57:35'),
	(3,'twitter','Twitter','http://www.twitter.com','2015-06-13 23:06:18','2015-06-14 06:57:56');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table states
# ------------------------------------------------------------

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_name` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;

INSERT INTO `states` (`id`, `slug`, `country_id`, `state_name`, `created_at`, `updated_at`)
VALUES
	(1,'anambra',1,'Anambra','2015-06-15 10:15:19','2015-06-15 10:15:19'),
	(4,'enugu',4,'Enugu','2015-06-15 10:19:13','2015-06-15 21:24:44'),
	(5,'akwa-ibom',1,'Akwa Ibom','2015-06-15 10:19:21','2015-06-15 10:19:21'),
	(6,'adamawa',1,'Adamawa','2015-06-15 10:19:33','2015-06-15 10:19:33');

/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `avatar`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Admin','Admin','codegap','blank','info@codegap.co.uk','$2y$10$Qh9GNUTsECP.usO4zrlELu3xcPq6veHmlJgCzncTjeCVkcf6Uv1g.','BCgDldX4p3HHusYCCRVd6OiFmZMoXGi1iRzURVdEsfqO0RBSpApE4isvRAJR','2015-06-13 01:27:08','2015-06-16 09:41:08');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
